/* Copyright (c) 2019-2019. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef INCLUDE_STATMC_HPP
#define INCLUDE_STATMC_HPP

#include <fstream>
#include <map>
#include <random>

namespace simgrid {
namespace statmc {
bool isActivated();
void init(std::string, std::string);
void read_input(int);
void read_input_start();
void read_input_ontimeadvance(double);
void send_variables();
void send_variables_onend();
void send_variables_ontimeadvance(double);
void send_rngstream_state();
void use_mersenne_statefile(std::string);
void save_mersenne_state();
void createIntVariable(std::string);
void createIntVariable(std::string, int);
void updateIntVariable(std::string, int);
int readIntVariable(std::string);
void createFloatVariable(std::string);
void createFloatVariable(std::string, float);
void updateFloatVariable(std::string, float);
float readFloatVariable(std::string);
} // namespace statmc

/*
namespace xbt {
namespace random {
void set_mersenne_state(std::string);
void read_mersenne_state(std::string);
} // namespace random
} // namespace xbt
*/
} // namespace simgrid

#endif
