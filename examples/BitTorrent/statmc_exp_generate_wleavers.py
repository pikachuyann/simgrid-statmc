#!/usr/bin/python
# -*- coding: utf-8 -*-

import os,signal
import subprocess,sys
import scipy.stats,numpy
import time

cwd=os.getcwd()
tmpdir=cwd+"/tmp-statmc-exp"
if (not os.path.exists(tmpdir)):
        os.makedirs(tmpdir)

singletimeout=50000
        
def handleSIGCHLD(a,b):
    os.waitpid(-1, os.WNOHANG)

signal.signal(signal.SIGCHLD, handleSIGCHLD)
        
def generer_scenario():
    seeders = 1 #Nombre de seeders au début de la simulation
    leechers = 75 #Nombre de leechers au début de la simulation
    peers = seeders + leechers
    isSeeder = [0]*peers; # Tableau (liste, certes) contenant si le pair est un seeder.
    # Choisir aléatoirement deux seeders parmi les peers :
    #for i in range(0,seeders):
    #    trouve=False
    #    while (not trouve):
    #        id=numpy.random.randint(0,peers)
    #        if (not isSeeder[id]):
    #            trouve=True
    #            isSeeder[id]=1
    isSeeder[0]=1
    #isSeeder[]=1
    # Créeons le fichier de plateforme
    platform=open(tmpdir+"/platform.xml","w")
    # D'abord avec les entêtes
    platform.write('<?xml version="1.0"?>')
    platform.write('<!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">')
    platform.write('<platform version="4.1">')
    # Maintenant la plateforme
    platform.write('<zone id="cluster0" routing="Full">')
    for i in range(0,peers+1):
        platform.write('<host id="node-'+str(i)+'.simgrid.org" speed="1Gf"')
        if (i > 0):
            platform.write(' state_file="state'+str(i)+'.txt"')
            state=open(tmpdir+"/state"+str(i)+".txt","w")
            state.write("STOCHASTIC LOOP\n");
            state.write("DET 0 DET 1\n");
            state.write("EXP 0.00001 DET 0\n");
            state.write("DET 10 DET 1\n");
        platform.write('/>')
        platform.write('<link id="node-'+str(i)+'.simgrid.org" bandwidth="1MBps" latency="50us" />')
    platform.write('<link id="cluster0_backbone" bandwidth="2.25GBps" latency="500us" sharing_policy="FATPIPE" />')
    for i in range(0,peers+1):
        for j in range(i+1,peers+1):
            platform.write('<route src="node-'+str(i)+'.simgrid.org" dst="node-'+str(j)+'.simgrid.org">')
            platform.write('<link_ctn id="node-'+str(i)+'.simgrid.org" />')
            platform.write('<link_ctn id="cluster0_backbone" />')
            platform.write('<link_ctn id="node-'+str(j)+'.simgrid.org" />')
            platform.write('</route>')
    # On ferme le fichier:
    platform.write('</zone>')
    platform.write('</platform>')
    platform.close()
    # Maintenons générons un fichier de déploiement
    deployment=open(tmpdir+"/deployment.xml","w")
    # Les entêtes:
    deployment.write('<?xml version="1.0"?>')
    deployment.write('<!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">')
    deployment.write('<platform version="4.1">')
    # Le contenu
    # a) Le tracker
    deployment.write('<actor host="node-0.simgrid.org" function="tracker">')
    deployment.write('<argument value="100" />') # Timeout de 100 par défaut
    deployment.write('</actor>')
    # b) Les peers
    for i in range(0,peers):
        hid=i+1
        deployment.write('<actor host="node-'+str(hid)+'.simgrid.org" function="peer" on_failure="RESTART">')
        deployment.write('<argument value="'+str(hid)+'"/> <argument value="'+str(singletimeout*0.95)+'" />') # ID identique, timeout de 50
        if (isSeeder[i]):
            deployment.write('<argument value="1" />') # C'est un seeder, on l'indique
        else:
            deployment.write('<prop id="leaver" value="true" />')
        deployment.write('</actor>')
    # La fin
    deployment.write('</platform>')
    deployment.close()

generer_scenario()
