/* Copyright (c) 2012-2019. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include <algorithm>
#include <climits>

#include "s4u-peer.hpp"
#include "s4u-tracker.hpp"
#include "xbt/random.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_bt_peer, "Messages specific for the peers");

/** Number of blocks asked by each request */
#define BLOCKS_REQUESTED 16UL

#define ENABLE_END_GAME_MODE 1
#define SLEEP_DURATION 0.1
#define BITS_TO_BYTES(x) (((x) / 8 + (x) % 8) ? 1 : 0)

Peer::Peer(std::vector<std::string> args)
{
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->onRestart(simgrid::s4u::Engine::get_clock());

  // Check arguments
  xbt_assert(args.size() == 3 || args.size() == 4, "Wrong number of arguments");
  try {
    id       = std::stoi(args[1]);
    mailbox_ = simgrid::s4u::Mailbox::by_name(std::to_string(id));
    mailbox_for_tracker = simgrid::s4u::Mailbox::by_name(std::to_string(id)+"TRCK");
  } catch (std::invalid_argument& ia) {
    throw std::invalid_argument(std::string("Invalid ID:") + args[1].c_str());
  }

  try {
    deadline = std::stod(args[2]);
    actual_deadline = deadline;
  } catch (std::invalid_argument& ia) {
    throw std::invalid_argument(std::string("Invalid deadline:") + args[2].c_str());
  }
  xbt_assert(deadline > 0, "Wrong deadline supplied");

  connected_peers = simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->getConnectedPeers();
  mailbox_ = simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->getMailbox();
  pieces_count = simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->getPiecesCount();

  if (simgrid::s4u::Actor::self()->get_property("simultaneous_unchokes") == nullptr) {
    simultaneous_unchokes = 1;
  } else {
    simultaneous_unchokes = std::stoi(simgrid::s4u::Actor::self()->get_property("simultaneous_unchokes"));
  }

  if (simgrid::s4u::Actor::self()->get_property("agressive_unchoke") == nullptr) {
    agressive_unchoke = false;
  } else {
    agressive_unchoke = true;
  }

  if (simgrid::s4u::Actor::self()->get_property("super_seeder") == nullptr) {
    super_seeder = false;
  } else {
    super_seeder = true;
  }

  if (not simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->hasStarted()) {
    simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->justStarted();
    if (args.size() == 4 && args[3] == "1") { // The peer is a seed
      bitfield_.set();      
      bitfield_blocks.set();
      seeder = true;
    }
  }
  else {
    bitfield_ = simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->getBitfield();
    bitfield_blocks = simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->getBitfieldBlocks();
    if (args.size() == 4 && args[3] == "1") {
      seeder = true;
    }
    // We must reset the "am_interested" value to default values to avoid further problems
    for (auto const& kv : connected_peers) {
      Connection* remote_peer = kv.second;
      remote_peer->am_interested = false;
    }
  }
}

Peer::~Peer()
{
//  if (not simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->hasCompleted()) {
  if (simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->currentlyInProgress()) {
    simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->nomoreInProgress();
    simgrid::statmc::updateIntVariable("inprogress", simgrid::statmc::readIntVariable("inprogress")-1);
  }
//  }
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->setConnectedPeers(connected_peers);
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->setPiecesCount(pieces_count);
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->setBitfield(bitfield_);
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->setBitfieldBlocks(bitfield_blocks);
//  for (auto const& peer : connected_peers)
//    delete peer.second;
//  delete[] pieces_count;
}

/** Peer main function */
void Peer::operator()()
{
//  XBT_INFO("Peer is launching");
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->onRestart(simgrid::s4u::Engine::get_clock());
  bool couldContactPeer=false;
  if (not (simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->hasCompleted())) {
  actual_deadline = simgrid::s4u::Engine::get_clock() + deadline;
  // Getting peer data from the tracker.
  if (getPeersFromTracker()) {
    couldContactPeer=true;
    XBT_DEBUG("Got %zu peers from the tracker. Current status is: %s", connected_peers.size(), getStatus().c_str());
  } // else { XBT_INFO("Couldn't contact tracker, will contact known peers anyway."); }
  if (not connected_peers.empty() || couldContactPeer) {
    if (not couldContactPeer) {
    bool shouldIcontinue = false; int continueFor=0;
        for (auto const& kv : connected_peers) {
          Connection* remote_peer = kv.second;
          if (remote_peer->id == id) { }
          else if (not remote_peer->has_finished) { shouldIcontinue = true; continueFor=remote_peer->id; break; }
        }
	XBT_INFO("Couldn't contact tracker, contacting known peers and continuing for %d",continueFor);
    }
    begin_receive_time = simgrid::s4u::Engine::get_clock();
    //mailbox_->set_receiver(simgrid::s4u::Actor::self());
    if (hasFinished()) {
      sendHandshakeToAllPeers();
      announceCompletion();
    } else {
      leech();
    }
    seed();
  } else { XBT_INFO("There was noone"); }

  XBT_INFO("Here is my current status: %s", getStatus().c_str());
  xbt_assert(hasFinished());
  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->justCompleted();
//  simgrid::s4u::this_actor::sleep_for(2000);
  } else {
    seed();
  }
}

bool Peer::getPeersFromTracker()
{
  simgrid::s4u::Mailbox* tracker_mailbox = simgrid::s4u::Mailbox::by_name(TRACKER_MAILBOX);
  // Build the task to send to the tracker
  TrackerQuery* peer_request = new TrackerQuery(id, MESSAGE_GETPAIRS, mailbox_for_tracker, mailbox_);
  try {
    XBT_DEBUG("Sending a peer request to the tracker.");
    tracker_mailbox->put(peer_request, TRACKER_COMM_SIZE, GET_PEERS_TIMEOUT);
  } catch (simgrid::TimeoutException& e) {
    XBT_DEBUG("Timeout expired when requesting peers to tracker");
    delete peer_request;
    return false;
  }

  try {
    TrackerAnswer* answer = static_cast<TrackerAnswer*>(mailbox_for_tracker->get(GET_PEERS_TIMEOUT));
    // Add the peers the tracker gave us to our peer list.
    for (auto const& peer_id : *answer->getPeers()) {
      if (id != peer_id)
          if (connected_peers.find(peer_id) == connected_peers.end())
              connected_peers[peer_id] = new Connection(peer_id,answer->getMailbox(peer_id));
    }
    delete answer;
  } catch (simgrid::TimeoutException& e) {
    XBT_DEBUG("Timeout expired when requesting peers to tracker");
    return false;
  }
  return true;
}

bool Peer::announceCompletion()
{
  if (not simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->hasCompleted()) {
	  simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->justCompleted();
  }
  XBT_INFO("I have finished!");
  simgrid::s4u::Mailbox* tracker_mailbox = simgrid::s4u::Mailbox::by_name(TRACKER_MAILBOX);
  // Build the task to send to the tracker
  TrackerQuery* peer_request = new TrackerQuery(id, MESSAGE_COMPLETED, mailbox_for_tracker, mailbox_);
  try {
    XBT_DEBUG("Sending completion information to the tracker.");
    tracker_mailbox->put(peer_request, TRACKER_COMM_SIZE, GET_PEERS_TIMEOUT);
  } catch (simgrid::TimeoutException& e) {
    XBT_DEBUG("Timeout expired when informing tracker I have completed");
    delete peer_request;
    return false;
  }
  
  for (auto const& kv : connected_peers) {
    Connection* remote_peer = kv.second;
    Message* handshake      = new Message(MESSAGE_COMPLETION, id, mailbox_);
    try {
      remote_peer->mailbox_->put_init(handshake, MESSAGE_COMPLETION_SIZE)->detach();
    } catch(simgrid::NetworkFailureException& ex) {
      XBT_INFO("Couldn't inform peer %d that I had finished.", message->peer_id);
    }
    XBT_DEBUG("Sending a COMPLETION to %d", remote_peer->id);
  }
  // simgrid::s4u::this_actor::sleep_for(2*SLEEP_DURATION);
  
  return true;
}

void Peer::sendHandshakeToAllPeers()
{
  for (auto const& kv : connected_peers) {
    Connection* remote_peer = kv.second;
    Message* handshake      = new Message(MESSAGE_HANDSHAKE, id, mailbox_);
    remote_peer->mailbox_->put_init(handshake, MESSAGE_HANDSHAKE_SIZE)->detach();
    XBT_DEBUG("Sending a HANDSHAKE to %d", remote_peer->id);
    if (hasFinished())  {
      Message* completion = new Message(MESSAGE_COMPLETION, id, mailbox_);
      remote_peer->mailbox_->put_init(completion, MESSAGE_COMPLETION_SIZE)->detach();
    }
  }
}

void Peer::sendMessage(simgrid::s4u::Mailbox* mailbox, e_message_type type, uint64_t size)
{
  const char* type_names[6] = {"HANDSHAKE", "CHOKE", "UNCHOKE", "INTERESTED", "NOTINTERESTED", "CANCEL"};
  XBT_DEBUG("Sending %s to %s", type_names[type], mailbox->get_cname());
  mailbox->put_init(new Message(type, id, bitfield_, mailbox_), size)->detach();
}

//void Peer::sendBitfield(simgrid::s4u::Mailbox* mailbox)
void Peer::sendBitfield(Connection* remote_peer)
{
  if (not super_seeder) {
    XBT_DEBUG("Sending a BITFIELD to %s", remote_peer->mailbox_->get_cname());
    remote_peer->mailbox_
        ->put_init(new Message(MESSAGE_BITFIELD, id, bitfield_, mailbox_),
                   MESSAGE_BITFIELD_SIZE + BITS_TO_BYTES(FILE_PIECES))
        ->detach();
  } else {
    XBT_DEBUG("Sending a lying BITFIELD to %s", remote_peer->mailbox_->get_cname());
    remote_peer->mailbox_
         ->put_init(new Message(MESSAGE_BITFIELD, id, remote_peer->superseed_announced_bitfield, mailbox_),
	            MESSAGE_BITFIELD_SIZE + BITS_TO_BYTES(FILE_PIECES))
	 ->detach();
  }
}

void Peer::sendPiece(simgrid::s4u::Mailbox* mailbox, unsigned int piece, int block_index, int block_length)
{
  xbt_assert(not hasNotPiece(piece), "Tried to send a unavailable piece.");
  XBT_DEBUG("Sending the PIECE %u (%d,%d) to %s", piece, block_index, block_length, mailbox->get_cname());
  mailbox->put_init(new Message(MESSAGE_PIECE, id, mailbox_, piece, block_index, block_length), block_length*BLOCK_SIZE)->detach();
  if (seeder) { simgrid::statmc::updateIntVariable("piecefromseeder", simgrid::statmc::readIntVariable("piecefromseeder")+1); }
}

void Peer::sendHaveToAllPeers(unsigned int piece)
{
  XBT_DEBUG("Sending HAVE message to all my peers");
  for (auto const& kv : connected_peers) {
    Connection* remote_peer = kv.second;
    remote_peer->mailbox_->put_init(new Message(MESSAGE_HAVE, id, mailbox_, piece), MESSAGE_HAVE_SIZE)->detach();
  }
}

void Peer::sendRequestTo(Connection* remote_peer, unsigned int piece)
{
  remote_peer->current_piece = piece;
  if (remote_peer->hasPiece(piece)) {
    int block_index = getFirstMissingBlockFrom(piece);
    if (block_index != -1) {
      int block_length = std::min(BLOCKS_REQUESTED, PIECES_BLOCKS - block_index);
      //XBT_INFO("[dbg] Requesting piece %d", piece);
      XBT_DEBUG("Sending a REQUEST to %s for piece %u (%d,%d)", remote_peer->mailbox_->get_cname(), piece, block_index,
                block_length);
      remote_peer->mailbox_
          ->put_init(new Message(MESSAGE_REQUEST, id, mailbox_, piece, block_index, block_length), MESSAGE_REQUEST_SIZE)
          ->detach();
    }
  } else {
    XBT_INFO("Aborting a sendRequestTo because I don't know if the remote peer actually has the requested piece.");
  }
}

std::string Peer::getStatus()
{
  return bitfield_.to_string();
}

bool Peer::hasFinished()
{
  return bitfield_.all();
}

/** Indicates if the remote peer has a piece not stored by the local peer */
bool Peer::isInterestedBy(Connection* remote_peer)
{
  std::bitset<FILE_PIECES> fullbitfield;
  fullbitfield.set();
  return (remote_peer->bitfield & (bitfield_ ^ fullbitfield)).any();
}

bool Peer::isInterestedByFree(Connection* remote_peer)
{
  for (unsigned int i = 0; i < FILE_PIECES; i++)
    if (hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i))
      return true;
  return false;
}

void Peer::updatePiecesCountFromBitfield(std::bitset<FILE_PIECES> bitfield)
{
  for (unsigned int i = 0; i < FILE_PIECES; i++)
    if (bitfield[i])
      pieces_count[i]++;
}

unsigned int Peer::countPieces(std::bitset<FILE_PIECES> bitfield)
{
  return bitfield.count();
}

int Peer::nbInterestedPeers()
{
  int nb = 0;
  for (auto const& kv : connected_peers)
    if (kv.second->interested)
      nb++;
  return nb;
}

void Peer::leech()
{
  if (not simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->currentlyInProgress()) {
    simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->nowInProgress();
    simgrid::statmc::updateIntVariable("inprogress", simgrid::statmc::readIntVariable("inprogress")+1);
  } else {
    XBT_INFO("Shouldn't be In Progress");
  }
  double next_choked_update = simgrid::s4u::Engine::get_clock() + UPDATE_CHOKED_INTERVAL;
  XBT_DEBUG("Start downloading.");

  /* Send a "handshake" message to all the peers it got (since it couldn't have gotten more than 50 peers) */
  sendHandshakeToAllPeers();
  XBT_DEBUG("Starting main leech loop listening on mailbox: %s", mailbox_->get_cname());

  void* data = nullptr;
  while (countPieces(bitfield_) < FILE_PIECES) {
    // Must stay alive if not completed.
    actual_deadline = simgrid::s4u::Engine::get_clock() + deadline; // Must stay alive a little more
    try {
    if (comm_received == nullptr) {
      comm_received = mailbox_->get_async(&data);
    }
    if (comm_received->test()) {
      message = static_cast<Message*>(data);
      handleMessage();
      delete message;
      comm_received = nullptr;
    } else {
      // We don't execute the choke algorithm if we don't already have a piece
      if (simgrid::s4u::Engine::get_clock() >= next_choked_update && countPieces(bitfield_) > 0) {
        updateChokedPeers();
        next_choked_update += UPDATE_CHOKED_INTERVAL;
      } else {
        simgrid::s4u::this_actor::sleep_for(SLEEP_DURATION);
      }
    }
    } catch(simgrid::NetworkFailureException& ex) {
      XBT_INFO("NFE during Leeching.");
      comm_received = nullptr;
      simgrid::s4u::this_actor::sleep_for(SLEEP_DURATION);
    }
  }
  if (hasFinished()) {
    if (simgrid::statmc::readIntVariable("isfirststilldownloading")>0) {
      simgrid::statmc::updateIntVariable("isfirststilldownloading",0);
    }
    XBT_DEBUG("%d becomes a seeder", id);
    simgrid::s4u::this_actor::get_host()->extension<HostBittorrent>()->nomoreInProgress();
    simgrid::statmc::updateIntVariable("inprogress", simgrid::statmc::readIntVariable("inprogress")-1);
    announceCompletion();
  }
}

void Peer::seed()
{
//  if (simgrid::s4u::this_actor::get_host()->get_property("leaver") == nullptr) {
  if (simgrid::s4u::Actor::self()->get_property("leaver") == nullptr) {
    double next_choked_update = simgrid::s4u::Engine::get_clock() + UPDATE_CHOKED_INTERVAL;
    XBT_DEBUG("Start seeding.");
    // start the main seed loop
    void* data = nullptr;
    while (simgrid::s4u::Engine::get_clock() < actual_deadline) {
      bool shouldIcontinue = false; int continueFor=0; bool everyoneCompleted=true;
          for (auto const& kv : connected_peers) {
            Connection* remote_peer = kv.second;
            if (remote_peer->id == id) { }
  	    else if (remote_peer->last_heard_of < simgrid::s4u::Engine::get_clock() - deadline) { everyoneCompleted=false; }
            else if (not remote_peer->has_finished) { shouldIcontinue = true; continueFor=remote_peer->id; break; }
          }
      if (not shouldIcontinue) { actual_deadline=deadline; }
      else { actual_deadline = simgrid::s4u::Engine::get_clock() + deadline; }
  
      // Must stay alive if there is at least one peer for which download is not finished
      if (not shouldIcontinue && simgrid::s4u::Engine::get_clock() > actual_deadline) {
        if (everyoneCompleted) {
          XBT_INFO("With the information I have, everyone has completed its task.");
        } // else {
//        XBT_INFO("Everyone I still had recent communication with has completed its task, timeouting.");
//      }
      }
      else {
        try {
          if (comm_received == nullptr) {
            comm_received = mailbox_->get_async(&data);
          }
          if (comm_received->test()) {
            message = static_cast<Message*>(data);
            handleMessage();
            delete message;
            comm_received = nullptr;
            lastreceived = simgrid::s4u::Engine::get_clock();
          } else {
            if (simgrid::s4u::Engine::get_clock() >= next_choked_update) {
              updateChokedPeers();
              // TODO: Change the choked peer algorithm when seeding.
              next_choked_update += UPDATE_CHOKED_INTERVAL;
            } else {
                simgrid::s4u::this_actor::sleep_for(SLEEP_DURATION);
            }
          }
        } catch(simgrid::NetworkFailureException& ex) {
          XBT_INFO("NFE during Seeding.");
          comm_received = nullptr;
          simgrid::s4u::this_actor::sleep_for(SLEEP_DURATION);
        }
      }

    }
  } else {
    XBT_INFO("I've finished downloading so I'll just leave.");
  }
}

void Peer::updateActivePeersSet(Connection* remote_peer)
{
  if (remote_peer->interested && not remote_peer->choked_upload)
    active_peers.insert(remote_peer);
  else
    active_peers.erase(remote_peer);
}

void Peer::handleMessage()
{
  const char* type_names[12] = {"HANDSHAKE", "CHOKE",    "UNCHOKE", "INTERESTED", "NOTINTERESTED",
                                "HAVE",      "BITFIELD", "REQUEST", "PIECE",      "CANCEL", "COMPLETION", "UNKNOWN"};

  XBT_DEBUG("Received a %s message from %s", type_names[message->type], message->return_mailbox->get_cname());

  auto known_peer         = connected_peers.find(message->peer_id);
  Connection* remote_peer = (known_peer == connected_peers.end()) ? nullptr : connected_peers[message->peer_id];
  if (remote_peer != nullptr || message->type == MESSAGE_HANDSHAKE || message->type == MESSAGE_UNKNOWN) {
/*  xbt_assert(remote_peer != nullptr || message->type == MESSAGE_HANDSHAKE,
             "The impossible did happened: A not-in-our-list peer sent us a message."); */

  if (remote_peer != nullptr) { remote_peer->last_heard_of=simgrid::s4u::Engine::get_clock(); }

  switch (message->type) {
    case MESSAGE_UNKNOWN:
      sendMessage(message->return_mailbox, MESSAGE_HANDSHAKE, MESSAGE_HANDSHAKE_SIZE);
      XBT_INFO("Apparently was unknown to peer, sending another handshake.");
      break;
    case MESSAGE_HANDSHAKE:
      // Check if the peer is in our connection list.
      if (remote_peer == nullptr) {
        XBT_DEBUG("This peer %d was unknown, answer to its handshake", message->peer_id);
	connected_peers.insert(std::pair<int,Connection*>(message->peer_id, new Connection(message->peer_id,message->return_mailbox,simgrid::s4u::Engine::get_clock())));
//        connected_peers[message->peer_id] = new Connection(message->peer_id,message->return_mailbox);
        sendMessage(message->return_mailbox, MESSAGE_HANDSHAKE, MESSAGE_HANDSHAKE_SIZE);
	remote_peer = connected_peers[message->peer_id];
	if (super_seeder) { super_seed_handshake(remote_peer); }
      }
      if (hasFinished()) {
        try {
          sendMessage(message->return_mailbox, MESSAGE_COMPLETION, MESSAGE_COMPLETION_SIZE);
        } catch(simgrid::NetworkFailureException& ex) {
          XBT_INFO("Couldn't inform peer %d that I had finished.", message->peer_id);
        }
      }
      // Send our bitfield to the peer
      //sendBitfield(message->return_mailbox);
      if (remote_peer == nullptr) {
        XBT_INFO("SHOULDN'T BE HERE, not sending bitfield.");
      } else {
        sendBitfield(remote_peer);
      }
      break;
    case MESSAGE_COMPLETION:
      remote_peer->has_finished = true;
      break;
    case MESSAGE_BITFIELD:
      // Update the pieces list
      updatePiecesCountFromBitfield(message->bitfield);
      // Store the bitfield
      remote_peer->bitfield = message->bitfield;
      if (remote_peer->am_interested) {
        XBT_INFO("Due to a NFE, a BITFIELD message has been received outside the normal protocol. Resetting value of am_interested.");
	remote_peer->am_interested=false;
      }
      if (isInterestedBy(remote_peer)) {
        remote_peer->am_interested = true;
        sendMessage(message->return_mailbox, MESSAGE_INTERESTED, MESSAGE_INTERESTED_SIZE);
      }
      break;
    case MESSAGE_INTERESTED:
      // Update the interested state of the peer.
      remote_peer->interested = true;
      // MODIFIED - Agressively unchoke whenever we're under the threshold of allowed simultaneous unchokes.
      if (remote_peer->choked_upload && active_peers.size() < simultaneous_unchokes) { unchokePeer(remote_peer); }
      else { updateActivePeersSet(remote_peer); } // unchokePeer will call updateActivePeersSet anyway.
      break;
    case MESSAGE_NOTINTERESTED:
      remote_peer->interested = false;
      updateActivePeersSet(remote_peer);
      break;
    case MESSAGE_UNCHOKE:
      //xbt_assert(remote_peer->choked_download);
      if (remote_peer->choked_download) {
        remote_peer->choked_download = false;
        // Send requests to the peer, since it has unchoked us
        if (remote_peer->am_interested)
          requestNewPieceTo(remote_peer);
      } else { XBT_INFO("A thrown away message from NFE resulted in an unexpected UNCHOKE"); }
      break;
    case MESSAGE_CHOKE:
      //xbt_assert(not remote_peer->choked_download);
      if (not remote_peer->choked_download) {
        remote_peer->choked_download = true;
        if (remote_peer->current_piece != -1)
          removeCurrentPiece(remote_peer, remote_peer->current_piece);
      } else { XBT_INFO("A thrown away message from NFE resulted in an unexpected CHOKE"); }
      break;
    case MESSAGE_HAVE:
      XBT_DEBUG("\t for piece %d", message->piece);
      xbt_assert((message->piece >= 0 && static_cast<unsigned int>(message->piece) < FILE_PIECES),
                 "Wrong HAVE message received");
      remote_peer->bitfield.set(message->piece);
      if (super_seeder) { super_seed_have(remote_peer, message->piece); }
      pieces_count[message->piece]++;
      // If the piece is in our pieces, we tell the peer that we are interested.
      if (not remote_peer->am_interested && hasNotPiece(message->piece)) {
        remote_peer->am_interested = true;
        sendMessage(message->return_mailbox, MESSAGE_INTERESTED, MESSAGE_INTERESTED_SIZE);
        if (not remote_peer->choked_download)
          requestNewPieceTo(remote_peer);
      }
      break;
    case MESSAGE_REQUEST:
      if (not remote_peer->interested) {
        XBT_INFO("The peer %i was not interested, ignoring a REQUEST.",remote_peer->id);
      } else {
      xbt_assert((message->piece >= 0 && static_cast<unsigned int>(message->piece) < FILE_PIECES),
                 "Wrong HAVE message received");
      if (not remote_peer->choked_upload) {
        XBT_DEBUG("\t for piece %d (%d,%d)", message->piece, message->block_index,
                  message->block_index + message->block_length);
        if (not hasNotPiece(message->piece)) {
          sendPiece(message->return_mailbox, message->piece, message->block_index, message->block_length);
        }
      } else {
        XBT_DEBUG("\t for piece %d but he is choked.", message->peer_id);
      }
      }
      break;
    case MESSAGE_PIECE:
      XBT_DEBUG(" \t for piece %d (%d,%d)", message->piece, message->block_index,
                message->block_index + message->block_length);
      if (remote_peer->choked_download) {
        XBT_INFO("Piece received by a chooked peer. Ignoring.");
      } else { 
      xbt_assert(remote_peer->am_interested || ENABLE_END_GAME_MODE,
                 "Can't received a piece if I'm not interested without end-game mode!"
                 "piece (%d) bitfield (%u) remote bitfield (%u)",
                 message->piece, bitfield_, remote_peer->bitfield);
      xbt_assert(not remote_peer->choked_download, "Can't received a piece if I'm choked !");
      xbt_assert((message->piece >= 0 && static_cast<unsigned int>(message->piece) < FILE_PIECES),
                 "Wrong piece received");
      // TODO: Execute a computation.
      if (hasNotPiece(static_cast<unsigned int>(message->piece))) {
        updateBitfieldBlocks(message->piece, message->block_index, message->block_length);
        if (hasCompletedPiece(static_cast<unsigned int>(message->piece))) {
          //XBT_INFO("[dbg] Received piece %d",message->piece);
          // Removing the piece from our piece list
          removeCurrentPiece(remote_peer, message->piece);
          // Setting the fact that we have the piece
          bitfield_.set(message->piece);
	  //XBT_INFO("[dbg] Received piece %d (%d%% - %u/%u)",message->piece,bitfield_.count()*100/FILE_PIECES,bitfield_.count(),FILE_PIECES);
          XBT_DEBUG("My status is now %s", getStatus().c_str());
          // Sending the information to all the peers we are connected to
          sendHaveToAllPeers(message->piece);
          // sending UNINTERESTED to peers that do not have what we want.
          updateInterestedAfterReceive();
	  // MODIFIED, have to check if OK with std; rerequest a piece if not choked.
	  if (not remote_peer->choked_download) {
            if (remote_peer->am_interested)
              requestNewPieceTo(remote_peer);
          }
        } else {                                      // piece not completed
          sendRequestTo(remote_peer, message->piece); // ask for the next block
        }
      } else {
        XBT_DEBUG("However, we already have it");
        xbt_assert(ENABLE_END_GAME_MODE, "Should not happen because we don't use end game mode !");
        requestNewPieceTo(remote_peer);
      }
      }
      break;
    case MESSAGE_CANCEL:
      break;
    default:
      THROW_IMPOSSIBLE;
  }
  // Update the peer speed.
  if (remote_peer) {
    remote_peer->addSpeedValue(1.0 / (simgrid::s4u::Engine::get_clock() - begin_receive_time));
  }
  begin_receive_time = simgrid::s4u::Engine::get_clock();
  } else {
    XBT_INFO("An unknown peer sent us an unexpected message.");
    sendMessage(message->return_mailbox, MESSAGE_UNKNOWN, MESSAGE_UNKNOWN_SIZE);
  }
}

/** Selects the appropriate piece to download and requests it to the remote_peer */
void Peer::requestNewPieceTo(Connection* remote_peer)
{
  int piece = selectPieceToDownload(remote_peer);
  if (piece != -1) {
    current_pieces |= (1U << (unsigned int)piece);
    sendRequestTo(remote_peer, piece);
  }
}

void Peer::removeCurrentPiece(Connection* remote_peer, unsigned int current_piece)
{
  current_pieces &= ~(1U << current_piece);
  remote_peer->current_piece = -1;
}

/** @brief Return the piece to be downloaded
 * There are two cases (as described in "Bittorrent Architecture Protocol", Ryan Toole :
 * If a piece is partially downloaded, this piece will be selected prioritarily
 * If the peer has strictly less than 4 pieces, he chooses a piece at random.
 * If the peer has more than pieces, he downloads the pieces that are the less replicated (rarest policy).
 * If all pieces have been downloaded or requested, we select a random requested piece (endgame mode).
 * @param remote_peer: information about the connection
 * @return the piece to download if possible. -1 otherwise
 */
int Peer::selectPieceToDownload(Connection* remote_peer)
{
  int piece = partiallyDownloadedPiece(remote_peer);
  // strict priority policy
  if (piece != -1)
    return piece;

  // end game mode
  if (countPieces(current_pieces) >= (FILE_PIECES - countPieces(bitfield_)) && isInterestedBy(remote_peer)) {
#if ENABLE_END_GAME_MODE == 0
    return -1;
#endif
    int nb_interesting_pieces = 0;
    // compute the number of interesting pieces
    for (unsigned int i = 0; i < FILE_PIECES; i++)
      if (hasNotPiece(i) && remote_peer->hasPiece(i))
        nb_interesting_pieces++;

    xbt_assert(nb_interesting_pieces != 0);
    // get a random interesting piece
    int random_piece_index = simgrid::xbt::random::uniform_int(0, nb_interesting_pieces - 1);
    int current_index      = 0;
    for (unsigned int i = 0; i < FILE_PIECES; i++) {
      if (hasNotPiece(i) && remote_peer->hasPiece(i)) {
        if (random_piece_index == current_index) {
          piece = i;
          break;
        }
        current_index++;
      }
    }
    xbt_assert(piece != -1);
    return piece;
  }
  // Random first policy
  if (countPieces(bitfield_) < 4 && isInterestedByFree(remote_peer)) {
    int nb_interesting_pieces = 0;
    // compute the number of interesting pieces
    for (unsigned int i = 0; i < FILE_PIECES; i++)
      if (hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i))
        nb_interesting_pieces++;
    xbt_assert(nb_interesting_pieces != 0);
    // get a random interesting piece
    int random_piece_index = simgrid::xbt::random::uniform_int(0, nb_interesting_pieces - 1);
    int current_index      = 0;
    for (unsigned int i = 0; i < FILE_PIECES; i++) {
      if (hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i)) {
        if (random_piece_index == current_index) {
          piece = i;
          break;
        }
        current_index++;
      }
    }
    xbt_assert(piece != -1);
    return piece;
  } else { // Rarest first policy
    short min         = SHRT_MAX;
    int nb_min_pieces = 0;
    int current_index = 0;
    // compute the smallest number of copies of available pieces
    for (unsigned int i = 0; i < FILE_PIECES; i++) {
      if (pieces_count[i] < min && hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i))
        min = pieces_count[i];
    }

    xbt_assert(min != SHRT_MAX || not isInterestedByFree(remote_peer));
    // compute the number of rarest pieces
    for (unsigned int i = 0; i < FILE_PIECES; i++)
      if (pieces_count[i] == min && hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i))
        nb_min_pieces++;

    xbt_assert(nb_min_pieces != 0 || not isInterestedByFree(remote_peer));
    // get a random rarest piece
    int random_rarest_index = 0;
    if (nb_min_pieces > 0) { random_rarest_index=simgrid::xbt::random::uniform_int(0, nb_min_pieces - 1); }
    for (unsigned int i = 0; i < FILE_PIECES; i++)
      if (pieces_count[i] == min && hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i)) {
        if (random_rarest_index == current_index) {
          piece = i;
          break;
        }
        current_index++;
      }

    xbt_assert(piece != -1 || not isInterestedByFree(remote_peer));
    return piece;
  }
}

void Peer::updateChokedPeers()
{
  if (nbInterestedPeers() == 0)
    return;
  XBT_DEBUG("(%d) update_choked peers %zu active peers", id, active_peers.size());
  // update the current round
  round_                  = (round_ + 1) % 3;
  Connection* chosen_peer = nullptr;
  // select first active peer and remove it from the set
  Connection* choked_peer;
  if (active_peers.empty() || active_peers.size() < simultaneous_unchokes) {
    choked_peer = nullptr;
  } else {
    choked_peer = *active_peers.begin();
    active_peers.erase(choked_peer);
  }

  // If we are currently seeding, we unchoke the peer which has been unchoked the last time.
  if (hasFinished()) {
    Connection* remote_peer;
    double unchoke_time = simgrid::s4u::Engine::get_clock() + 1;
    for (auto const& kv : connected_peers) {
      remote_peer = kv.second;
      // MODIFIED: Allow unchoked peers to still be unchoked.
      if (remote_peer->last_unchoke < unchoke_time && remote_peer->interested && (remote_peer->choked_upload || remote_peer == choked_peer)) {
        unchoke_time = remote_peer->last_unchoke;
        chosen_peer  = remote_peer;
      }
    }
  } else {
    // Random optimistic unchoking
    if (round_ == 0) {
      int j = 0;
      do {
        // We choose a random peer to unchoke.
        std::map<int, Connection*>::iterator chosen_peer_it = connected_peers.begin();
        std::advance(chosen_peer_it, simgrid::xbt::random::uniform_int(0, connected_peers.size() - 1));
        chosen_peer = chosen_peer_it->second;
        if (chosen_peer == nullptr)
          xbt_assert(false, "A peer should have be selected at this point");
        else if (not chosen_peer->interested || not chosen_peer->choked_upload)
          chosen_peer = nullptr;
        else
          XBT_DEBUG("Nothing to do, keep going");
        j++;
      } while (chosen_peer == nullptr && j < MAXIMUM_PEERS);
    } else {
      // Use the "fastest download" policy.
      double fastest_speed = 0.0;
      for (auto const& kv : connected_peers) {
        Connection* remote_peer = kv.second;
        if (remote_peer->peer_speed > fastest_speed && remote_peer->choked_upload && remote_peer->interested) {
          chosen_peer   = remote_peer;
          fastest_speed = remote_peer->peer_speed;
        }
      }
    }
  }

  if (chosen_peer != nullptr)
    XBT_DEBUG("(%d) update_choked peers unchoked (%d) ; int (%d) ; choked (%d) ", id, chosen_peer->id,
              chosen_peer->interested, chosen_peer->choked_upload);

  if (choked_peer != chosen_peer) {
    if (choked_peer != nullptr) { chokePeer(choked_peer); }
    if (chosen_peer != nullptr) { unchokePeer(chosen_peer); }
  } else { // MODIFIED: In the case the currently chosen peer and peer to unchoke are the same, we should update the last unchoke time.
    if (chosen_peer != nullptr) {
      chosen_peer->last_unchoke = simgrid::s4u::Engine::get_clock();
    }
  }
}

void Peer::chokePeer(Connection* peer) {
  xbt_assert(not peer->choked_upload,"Tries to choke a choked peer");
  peer->choked_upload=true;
  updateActivePeersSet(peer);
  XBT_DEBUG("(%d) Sending a CHOKE to %d", id, peer->id);
  sendMessage(peer->mailbox_, MESSAGE_CHOKE, MESSAGE_CHOKE_SIZE);
}

void Peer::unchokePeer(Connection* peer) {
  xbt_assert(peer->choked_upload, "Tries to unchoke an unchoked peer");
  peer->choked_upload=false;
  active_peers.insert(peer);
  peer->last_unchoke = simgrid::s4u::Engine::get_clock();
  XBT_DEBUG("(%d) Sending a UNCHOKE to %d", id, peer->id);
  updateActivePeersSet(peer);
  sendMessage(peer->mailbox_, MESSAGE_UNCHOKE, MESSAGE_UNCHOKE_SIZE);
}

/** @brief Update "interested" state of peers: send "not interested" to peers that don't have any more pieces we want.*/
void Peer::updateInterestedAfterReceive()
{
  for (auto const& kv : connected_peers) {
    Connection* remote_peer = kv.second;
    if (remote_peer->am_interested) {
      bool interested = false;
      // Check if the peer still has a piece we want.
      for (unsigned int i = 0; i < FILE_PIECES; i++)
        if (hasNotPiece(i) && remote_peer->hasPiece(i)) {
          interested = true;
          break;
        }

      if (not interested) { // no more piece to download from connection
        remote_peer->am_interested = false;
        sendMessage(remote_peer->mailbox_, MESSAGE_NOTINTERESTED, MESSAGE_NOTINTERESTED_SIZE);
      }
    }
  }
}

void Peer::updateBitfieldBlocks(int piece, int block_index, int block_length)
{
  xbt_assert((piece >= 0 && static_cast<unsigned int>(piece) <= FILE_PIECES), "Wrong piece.");
  xbt_assert((block_index >= 0 && static_cast<unsigned int>(block_index) <= PIECES_BLOCKS), "Wrong block : %d.",
             block_index);
  for (int i = block_index; i < (block_index + block_length); i++)
    bitfield_blocks.set(piece * PIECES_BLOCKS + i);
}

bool Peer::hasCompletedPiece(unsigned int piece)
{
  for (unsigned int i = 0; i < PIECES_BLOCKS; i++)
    if (not(bitfield_blocks[piece * PIECES_BLOCKS + i]))
      return false;
  return true;
}

int Peer::getFirstMissingBlockFrom(int piece)
{
  for (unsigned int i = 0; i < PIECES_BLOCKS; i++)
    if (not(bitfield_blocks[piece * PIECES_BLOCKS + i]))
      return i;
  return -1;
}

/** Returns a piece that is partially downloaded and stored by the remote peer if any -1 otherwise. */
int Peer::partiallyDownloadedPiece(Connection* remote_peer)
{
  for (unsigned int i = 0; i < FILE_PIECES; i++)
    if (hasNotPiece(i) && remote_peer->hasPiece(i) && isNotDownloadingPiece(i) && getFirstMissingBlockFrom(i) > 0)
      return i;
  return -1;
}

/** Super seeding functions */

/** Adapted from libtorrent's get_piece_to_super_seed */
int Peer::super_seed_getpiecetosuperseed() {
  int min_availability = 9999;
  std::vector<unsigned int> avail_vec;
  Connection* remote_peer = nullptr;

  for (unsigned int i = 0; i < FILE_PIECES;i++) {
    int availability = 0;
    for (auto const& kv : connected_peers) {
      remote_peer = kv.second;
      if (remote_peer->superseed_announced_bitfield[i]) {
        availability = 999;
	break;
      }
      if (remote_peer->bitfield[i]) { ++availability; }
    }
    if (availability > min_availability) continue;
    if (availability == min_availability) {
      avail_vec.push_back(i);
      continue;
    }
    min_availability = availability;
    avail_vec.clear();
    avail_vec.push_back(i);
  }

  if (avail_vec.empty()) { return 0; } // Returned piece_index_t(-1) in libtorrent; probably should make sure to give a better default value here !
//  else { return avail_vec[ simgrid::xbt::random::uniform_int(0, avail_vec.size() - 1) ]; }
  else { return avail_vec[0]; }
}

/** Forces one piece to be announced to the remote peer upon handshake */
void Peer::super_seed_handshake(Connection* remote_peer) {
  if (remote_peer->superseed_announced_bitfield.count() > 0) { }
  else {
    unsigned int hs_piece = super_seed_getpiecetosuperseed(); 
    remote_peer->superseed_announced_bitfield.set( hs_piece );
  }
}


void Peer::super_seed_have(Connection* remote_peer, unsigned int piece) {
  if (remote_peer->superseed_announced_bitfield[piece]) { /* Do nothing. */ }
  else {
    // That means the piece has been announced somewhere else and we can send a new piece to peers who downloaded the superseeded piece
    Connection* other_peer = nullptr;
    for (auto const& kv : connected_peers) {
      other_peer = kv.second;
      if (other_peer->superseed_announced_bitfield[piece]) {
        unsigned int other_piece = super_seed_getpiecetosuperseed();
	other_peer->superseed_announced_bitfield.set( other_piece );
	other_peer->mailbox_->put_init(new Message(MESSAGE_HAVE, id, mailbox_, other_piece), MESSAGE_HAVE_SIZE)->detach();
      }
    }
  }
}
