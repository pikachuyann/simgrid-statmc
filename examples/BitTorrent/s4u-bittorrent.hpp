/* Copyright (c) 2012-2019. The SimGrid Team.
 * All rights reserved.                                                     */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef BITTORRENT_BITTORRENT_HPP_
#define BITTORRENT_BITTORRENT_HPP_

#include <simgrid/s4u.hpp>
#include "../../include-statmc.hpp"

#include <bitset>

#define MAILBOX_SIZE 40
#define TRACKER_MAILBOX "tracker_mailbox"
/** Max number of peers sent by the tracker to clients */
#define MAXIMUM_PEERS 50
/** Interval of time where the peer should send a request to the tracker */
#define TRACKER_QUERY_INTERVAL 1000
/** Communication size for a task to the tracker */
#define TRACKER_COMM_SIZE 1
#define GET_PEERS_TIMEOUT 10000
#define TIMEOUT_MESSAGE 10
#define TRACKER_RECEIVE_TIMEOUT 10
/** Number of peers that can be unchocked at a given time */
#define MAX_UNCHOKED_PEERS 4
/** Interval between each update of the choked peers */
#define UPDATE_CHOKED_INTERVAL 30
/** Number of pieces the peer asks for simultaneously */
#define MAX_PIECES 1

/** Message sizes
 * Sizes based on report by A. Legout et al, Understanding BitTorrent: An Experimental Perspective
 * http://hal.inria.fr/inria-00000156/en
 */
#define MESSAGE_HANDSHAKE_SIZE 68
#define MESSAGE_CHOKE_SIZE 5
#define MESSAGE_UNCHOKE_SIZE 5
#define MESSAGE_INTERESTED_SIZE 5
#define MESSAGE_NOTINTERESTED_SIZE 5
#define MESSAGE_HAVE_SIZE 9
#define MESSAGE_BITFIELD_SIZE 5
#define MESSAGE_REQUEST_SIZE 17
#define MESSAGE_PIECE_SIZE 13
#define MESSAGE_CANCEL_SIZE 17
/** This specific message size has been added for a very specific purpose */
#define MESSAGE_COMPLETION_SIZE 5
/** This specific message size has been added for another specific purpose (please send me another handshake !) **/
#define MESSAGE_UNKNOWN_SIZE 5

/** Size and data for the downloaded file: **/
/* 222 -> 55.5 MB ; 1400 -> 350MB */
#define FILE_PIECES 100UL
#define PIECES_BLOCKS 16UL
#define BLOCK_SIZE 65536
/* 16384 x 16 x 222 = 55.5MB #SuperSeeding */
/* 16384 x 16 x 1400 = 350MB */
/* 65536 x 16 x 100 = 100 MB #heterogeneous tcp/utp swarms */

/** Types of messages exchanged between two peers. */
enum e_message_type {
  MESSAGE_HANDSHAKE,
  MESSAGE_CHOKE,
  MESSAGE_UNCHOKE,
  MESSAGE_INTERESTED,
  MESSAGE_NOTINTERESTED,
  MESSAGE_HAVE,
  MESSAGE_BITFIELD,
  MESSAGE_REQUEST,
  MESSAGE_PIECE,
  MESSAGE_CANCEL,
  MESSAGE_COMPLETION,
  MESSAGE_UNKNOWN
};

class Message {
public:
  e_message_type type;
  int peer_id;
  simgrid::s4u::Mailbox* return_mailbox;
  std::bitset<FILE_PIECES> bitfield; /* Default value is 0 everywhere */
  int piece             = 0;
  int block_index       = 0;
  int block_length      = 0;
  Message(e_message_type type, int peer_id, simgrid::s4u::Mailbox* return_mailbox)
      : type(type), peer_id(peer_id), return_mailbox(return_mailbox){};
  Message(e_message_type type, int peer_id, std::bitset<FILE_PIECES> bitfield, simgrid::s4u::Mailbox* return_mailbox)
      : type(type), peer_id(peer_id), return_mailbox(return_mailbox), bitfield(bitfield){};
  Message(e_message_type type, int peer_id, simgrid::s4u::Mailbox* return_mailbox, int piece, int block_index,
          int block_length)
      : type(type)
      , peer_id(peer_id)
      , return_mailbox(return_mailbox)
      , piece(piece)
      , block_index(block_index)
      , block_length(block_length){};
  Message(e_message_type type, int peer_id, simgrid::s4u::Mailbox* return_mailbox, int piece)
      : type(type), peer_id(peer_id), return_mailbox(return_mailbox), piece(piece){};
  ~Message() = default;
};

class Connection {
public:
  int id; // Peer id
  simgrid::s4u::Mailbox* mailbox_;
  std::bitset<FILE_PIECES> bitfield; // Fields
  std::bitset<FILE_PIECES> superseed_announced_bitfield;
  //  int messages_count;
  double peer_speed    = 0;
  double last_unchoke  = 0;
  int current_piece    = -1;
  bool am_interested   = false; // Indicates if we are interested in something the peer has
  bool interested      = false; // Indicates if the peer is interested in one of our pieces
  bool choked_upload   = true;  // Indicates if the peer is choked for the current peer
  bool choked_download = true;  // Indicates if the peer has choked the current peer
  bool has_finished = false;
  double last_heard_of = 0;

//  explicit Connection(int id) : id(id), mailbox_(simgrid::s4u::Mailbox::by_name(std::to_string(id))){};
  explicit Connection(int id, simgrid::s4u::Mailbox* mb): id(id), mailbox_(mb){};
  explicit Connection(int id, simgrid::s4u::Mailbox* mb, double time): id(id), mailbox_(mb), last_heard_of(time){};
  ~Connection() = default;
  void addSpeedValue(double speed) { peer_speed = peer_speed * 0.6 + speed * 0.4; }
  bool hasPiece(unsigned int piece) { return bitfield[piece]; }
};


class HostBittorrent {
  simgrid::s4u::Host* host = nullptr;
  simgrid::s4u::Mailbox* host_mailbox;
  short* pieces_count;
  bool completed_task=false;
  bool started_task=false;
  bool currently_inprogress=false;
  std::bitset<FILE_PIECES> bitfield_;
  std::bitset<FILE_PIECES*PIECES_BLOCKS> bitfield_blocks;
  double lastrestart=0;

public:
  static simgrid::xbt::Extension<simgrid::s4u::Host, HostBittorrent> EXTENSION_ID;

  explicit HostBittorrent(simgrid::s4u::Host* ptr) : host(ptr)
  {
    std::string descr = std::string("RngSream<") + host->get_cname() + ">";
    host_mailbox = simgrid::s4u::Mailbox::by_name(host->get_cname());
    pieces_count = new short[FILE_PIECES]{0};
    if (simgrid::statmc::isActivated()) {
      simgrid::statmc::updateIntVariable("servers", simgrid::statmc::readIntVariable("servers")+1);
    }
  }
  std::map<int, Connection*> connected_peers;

  ~HostBittorrent() { };

  simgrid::s4u::Mailbox* getMailbox() { return host_mailbox; }
  void setConnectedPeers(std::map<int, Connection*> actor_cp) { lastrestart=simgrid::s4u::Engine::get_clock(); connected_peers = actor_cp; }
  std::map<int, Connection*> getConnectedPeers() { return connected_peers; }
  void setPiecesCount(short* pc) { lastrestart=simgrid::s4u::Engine::get_clock(); pieces_count = pc; }
  short* getPiecesCount() { return pieces_count; }
  bool hasCompleted() { return completed_task; }
  void justCompleted() {
	  if (simgrid::statmc::isActivated() && not completed_task) {
		  simgrid::statmc::updateIntVariable("completed", simgrid::statmc::readIntVariable("completed")+1);
//		  simgrid::statmc::updateIntVariable("inprogress", simgrid::statmc::readIntVariable("inprogress")-1);
	  }
	  lastrestart=simgrid::s4u::Engine::get_clock(); completed_task=true;
  }
  bool hasStarted() { return started_task; }
  void justStarted() { lastrestart=simgrid::s4u::Engine::get_clock(); started_task=true; }
  bool currentlyInProgress() { return currently_inprogress; }
  void nowInProgress() { currently_inprogress=true; }
  void nomoreInProgress() { currently_inprogress=false; }
  std::bitset<FILE_PIECES> getBitfield() { return bitfield_; }
  void setBitfield(std::bitset<FILE_PIECES> bf) { bitfield_=bf; }
  std::bitset<FILE_PIECES*PIECES_BLOCKS> getBitfieldBlocks() { return bitfield_blocks; }
  void setBitfieldBlocks(std::bitset<FILE_PIECES*PIECES_BLOCKS> bfb) { bitfield_blocks=bfb; }
  void onRestart(double time) {
	if (lastrestart > time) {
		started_task=false;
		std::map<int,Connection*> empty_connected;
		connected_peers=empty_connected;
		bitfield_.reset(); bitfield_blocks.reset();
		completed_task=false; started_task=false;
		pieces_count=new short[FILE_PIECES]{0};
	} else { lastrestart = time; }
  }
};

#endif /* BITTORRENT_BITTORRENT_HPP_ */
