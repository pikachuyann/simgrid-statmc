/* Copyright (c) 2012-2019. The SimGrid Team.
 * All rights reserved.                                                     */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "s4u-tracker.hpp"
#include <algorithm>
#include "xbt/random.hpp"

#define TRCK_SLEEP_DURATION 0.1

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_bt_tracker, "Messages specific for the tracker");

Tracker::Tracker(std::vector<std::string> args)
{
  // Checking arguments
  xbt_assert(args.size() == 2, "Wrong number of arguments for the tracker.");
  // Retrieving end time
  try {
    deadline = std::stod(args[1]);
  } catch (std::invalid_argument& ia) {
    throw std::invalid_argument(std::string("Invalid deadline:") + args[1].c_str());
  }
  xbt_assert(deadline > 0, "Wrong deadline supplied");

  mailbox = simgrid::s4u::Mailbox::by_name(TRACKER_MAILBOX);

  if (simgrid::statmc::isActivated()) {
    simgrid::statmc::updateIntVariable("servers", simgrid::statmc::readIntVariable("servers")-1);
  } // FixMe : The previous line is valid if, and only if, the tracker doesn't crash.
  XBT_INFO("Tracker launched.");
}

void Tracker::operator()()
{
  simgrid::s4u::CommPtr comm = nullptr;
  void* received             = nullptr;
  while (simgrid::s4u::Engine::get_clock() < deadline || known_peers.size() > finished_peers.size()) {
    try {
    if (comm == nullptr)
      comm = mailbox->get_async(&received);
    if (comm->test()) {
      // Retrieve the data sent by the peer.
      xbt_assert(received != nullptr);
      TrackerQuery* tq = static_cast<TrackerQuery*>(received);

      // Add the peer to our peer list, if not already known.
      if (seen_peers.find(tq->getPeerId()) == seen_peers.end()) {
        seen_peers.insert(tq->getPeerId());
        XBT_INFO("[Tracker] A new peer has appeared. Completion status: %d/%d",finished_peers.size(),seen_peers.size());
      }
      
      if (tq->type == MESSAGE_GETPAIRS) {
        if (known_peers.find(tq->getPeerId()) == known_peers.end()) {
          known_peers.insert(tq->getPeerId());
	  known_peers_mailboxes.insert(std::pair<int,simgrid::s4u::Mailbox*>(tq->getPeerId(),tq->getPeerMailbox()));
          /* It is added to the list of peers that have checked what other peers are available */
          /* I am not sure why it has to be different from seen_peers definition so far */
        }
        // Sending back peers to the requesting peer
        TrackerAnswer* ta = new TrackerAnswer(TRACKER_QUERY_INTERVAL);
        std::set<int>::iterator next_peer;
        int nb_known_peers = known_peers.size();
        int max_tries      = std::min(MAXIMUM_PEERS, nb_known_peers);
        int tried          = 0;
        while (tried < max_tries) {
          do {
            next_peer = known_peers.begin();
            std::advance(next_peer, simgrid::xbt::random::uniform_int(0, nb_known_peers - 1));
          } while (ta->getPeers()->find(*next_peer) != ta->getPeers()->end());
          ta->addPeer(*next_peer, known_peers_mailboxes[*next_peer]);
          tried++;
        }
	//XBT_INFO("Number of pairs: %i (asked by %i)",ta->getPeers()->size(),tq->getPeerId());
        tq->getReturnMailbox()->put_init(ta, TRACKER_COMM_SIZE)->detach();

      } else if (tq->type == MESSAGE_COMPLETED) {
        /* Says the corresponding peer has completed; adding it to the list if not known */
        if (finished_peers.find(tq->getPeerId()) == finished_peers.end()) {
          finished_peers.insert(tq->getPeerId());
          XBT_INFO("[Tracker] A peer has completed its file. Completion status: %d/%d",finished_peers.size(),seen_peers.size());
        }
      }
      delete tq;
      comm = nullptr;
    } else {
      simgrid::s4u::this_actor::sleep_for(TRCK_SLEEP_DURATION);
    }
    } catch(simgrid::NetworkFailureException& ex) {
      XBT_INFO("Tracker NFE, throwing message away.");
      comm=nullptr;
      simgrid::s4u::this_actor::sleep_for(TRCK_SLEEP_DURATION);
    }
  }
  XBT_INFO("Tracker is leaving");
}
