#/bin/bash

CXX="g++" 
CXX_INCLUDES="-I/usr/local/include -I/usr/local/include/graphviz -I/opt/local/include"
CXX_FLAGS="-g3 -O3 -std=gnu++11"

$CXX $CXX_INCLUDES $CXX_FLAGS -o include-statmc.cpp.o -c ../../include-statmc.cpp
$CXX $CXX_INCLUDES $CXX_FLAGS -o s4u-peer.cpp.o -c s4u-peer.cpp
$CXX $CXX_INCLUDES $CXX_FLAGS -o s4u-tracker.cpp.o -c s4u-tracker.cpp
$CXX $CXX_INCLUDES $CXX_FLAGS -o s4u-bittorrent.cpp.o -c s4u-bittorrent.cpp
$CXX $CXX_INCLUDES $CXX_FLAGS -E s4u-bittorrent.cpp > s4u-bittorrent.cpp.i
$CXX $CXX_INCLUDES $CXX_FLAGS -S s4u-bittorrent.cpp -o s4u-bittorrent.cpp.s
$CXX $CXX_INCLUDES $CXX_FLAGS -o s4u-bittorrent-statmc.cpp.o -c s4u-bittorrent-statmc.cpp
$CXX $CXX_INCLUDES $CXX_FLAGS -E s4u-bittorrent-statmc.cpp > s4u-bittorrent-statmc.cpp.i
$CXX $CXX_INCLUDES $CXX_FLAGS -S s4u-bittorrent-statmc.cpp -o s4u-bittorrent-statmc.cpp.s
$CXX $CXX_INCLUDES $CXX_FLAGS include-statmc.cpp.o s4u-bittorrent.cpp.o s4u-peer.cpp.o s4u-tracker.cpp.o -o s4u-bittorrent -rdynamic /usr/local/lib/libsimgrid.so -lm -lunwind-ptrace -lunwind-generic -lunwind -lpthread -lrt /usr/lib/x86_64-linux-gnu/libdl.so -Wl,--as-needed -latomic -Wl,-no-as-needed -Wl,-rpath,/usr/local/lib/simgrid
$CXX $CXX_INCLUDES $CXX_FLAGS include-statmc.cpp.o s4u-bittorrent-statmc.cpp.o s4u-peer.cpp.o s4u-tracker.cpp.o -o s4u-bittorrent-statmc -rdynamic /usr/local/lib/libsimgrid.so -lm -lunwind-ptrace -lunwind-generic -lunwind -lpthread -lrt /usr/lib/x86_64-linux-gnu/libdl.so -Wl,--as-needed -latomic -Wl,-no-as-needed -Wl,-rpath,/usr/local/lib/simgrid
rm *.i *.o *.s #Clean-up
