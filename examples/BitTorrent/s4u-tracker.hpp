/* Copyright (c) 2012-2019. The SimGrid Team.
 * All rights reserved.                                                     */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef BITTORRENT_TRACKER_HPP_
#define BITTORRENT_TRACKER_HPP_

#include "s4u-bittorrent.hpp"
#include <set>

/** Types of messages exchanged with the tracker */
enum tr_message_type {
  MESSAGE_GETPAIRS,
  MESSAGE_COMPLETED
};

class TrackerQuery {
  int peer_id; // peer id
  simgrid::s4u::Mailbox* return_mailbox;
  simgrid::s4u::Mailbox* peer_mailbox;
public:
  tr_message_type type; // message type for trackers
  explicit TrackerQuery(int peer_id, tr_message_type msg, simgrid::s4u::Mailbox* return_mailbox, simgrid::s4u::Mailbox* peer_mailbox)
      : peer_id(peer_id), type(msg), return_mailbox(return_mailbox), peer_mailbox(peer_mailbox){};
  ~TrackerQuery() = default;
  int getPeerId() { return peer_id; }
  simgrid::s4u::Mailbox* getPeerMailbox() { return peer_mailbox; }
  simgrid::s4u::Mailbox* getReturnMailbox() { return return_mailbox; }
};

class TrackerAnswer {
  // int interval; // how often the peer should contact the tracker (unused for now)
  std::set<int>* peers; // the peer list the peer has asked for.
  std::map<int,simgrid::s4u::Mailbox*> peers_mailboxes; // the set of mailboxes to contact
public:
  explicit TrackerAnswer(int /*interval*/) /*: interval(interval)*/ { peers = new std::set<int>; }
  TrackerAnswer(const TrackerAnswer&)                                       = delete;
  TrackerAnswer& operator=(const TrackerAnswer&) = delete;
  ~TrackerAnswer() { delete peers; };
  void addPeer(int peer, simgrid::s4u::Mailbox* mailbox) { peers->insert(peer); peers_mailboxes.insert(std::pair<int,simgrid::s4u::Mailbox*>(peer,mailbox)); }
  simgrid::s4u::Mailbox* getMailbox(int id) { return peers_mailboxes[id]; }
  std::set<int>* getPeers() { return peers; }
};

class Tracker {
  double deadline;
  simgrid::s4u::Mailbox* mailbox;
  std::set<int> seen_peers;
  std::set<int> known_peers;
  std::map<int,simgrid::s4u::Mailbox*> known_peers_mailboxes;
  std::set<int> finished_peers;

public:
  explicit Tracker(std::vector<std::string> args);
  void operator()();
};

#endif /* BITTORRENT_TRACKER_HPP */
