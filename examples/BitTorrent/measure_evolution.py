#!/usr/bin/python
# -*- coding: utf-8 -*-

import os,signal
import subprocess,sys
import time

cwd=os.getcwd()
platfdir=cwd+"/tmp-statmc-exp" #Where is the scenario information stored?
tmpdir=cwd+"/tmp" #Where will we store our communication files?
resultfile=cwd+"/evolution.txt"
simulatorexe=cwd+"/s4u-bittorrent-statmc"
nbSimulators = 8 #number of parallel simulators
totSimulations = 10

dbgMode = False

def handleSIGCHLD(a,b):
    os.waitpid(-1, os.WNOHANG)

signal.signal(signal.SIGCHLD, handleSIGCHLD)


def main():
    children = False
    if (not os.path.exists(tmpdir+"/tmpresults")):
        os.mkfifo(tmpdir+"/tmpresults")
    print("Starting "+str(nbSimulators)+" parallel simulator processes")
    for i in range(1,nbSimulators+1):
        if (not children):
            newpid=os.fork()
            if newpid==0:
                children = True
                children_id = i
    if children:
        children_main(children_id)
    else:
        father_main()

def children_main(myid):
    # Create the FIFOs if necessary
    if (not os.path.exists(tmpdir+"/fifoin"+str(myid))):
        os.mkfifo(tmpdir+"/fifoin"+str(myid))
    if (not os.path.exists(tmpdir+"/fifoout"+str(myid))):
        os.mkfifo(tmpdir+"/fifoout"+str(myid))
    # Main loop:
    try:
        with open(tmpdir+"/tmpresults","w",1) as results:
            while (True):
                newpid = os.fork()
                lastnb = 0
                lasttime = 0.0
                if newpid==0:
                    # In this case, we do nothing except for launching the simulator.
                    # Note that the simulator must be launched with 7 parameters, in that order: platform_file deployment_file default_seed mtstatefile fifoin fifoout
                    simulator = subprocess.Popen( [simulatorexe, platfdir+"/platform.xml", platfdir+"/deployment.xml", str(myid), tmpdir+"/mtstate"+str(myid)+".txt", tmpdir+"/fifoin"+str(myid), tmpdir+"/fifoout"+str(myid)], stdout=subprocess.PIPE, stderr=subprocess.PIPE )
                    (output, err) = simulator.communicate()
                    errorcode = simulator.wait()
                    os._exit(0) # Once the simulator has finished its task, we close the fork.
                else:
                    time.sleep(1) # A workaround.
                    completed=0
                    servers=0
                    myresultline="0 "
                    with open(tmpdir+"/fifoin"+str(myid),"w",1) as fifoin:
                          with open(tmpdir+"/fifoout"+str(myid),"r",1) as fifoout:
                                fifoin.write("START\n")
                                fifoin.flush()
                                contreading = True
                                notAlreadyFinished = True
                                alreadyStopped = False
                                timeouted = False
                                while contreading:
                                    line = fifoout.readline()
                                    elements = line.strip()
                                    elements = elements.split(" ")
                                    if (elements[0] == "T"):
                                        for i in range(2,len(elements)):
                                            elem = elements[i].split("=")
                                            if dbgMode:
                                                print(elem)
                                            if (elem[0]=="completed"):
                                                newcompleted=int(elem[1])
                                                if (completed < newcompleted):
                                                    for i in range(completed, newcompleted): #In the case we jump several `completed` within one step…
                                                        myresultline+=elements[1]+" "
                                                    completed=newcompleted
                                            if (elem[0]=="servers"):
                                                servers=int(elem[1])
                                        if (completed==servers):
                                            if (notAlreadyFinished):
                                                results.write(myresultline+"\n")
                                                results.flush()
                                                notAlreadyFinished = False
                                                contreading = False
                                                fifoin.write("STOP\n")
                                                fifoin.flush()
                                        if dbgMode:
                                            print("(I'm "+str(myid)+") : At "+elements[1]+" I have "+str(completed)+" out of "+str(servers))
                                        if contreading:
                                            fifoin.write("CONT\n")
                                            fifoin.flush()
                                    if (elements[0] == "END"):
                                        contreading = False
    except IOError, e:
        exit(0)
    except KeyboardInterrupt, e:
        exit(0)

# which concludes children_main().

def father_main():
    # This hasn't much to do, it should count the number of received results lines and gather them into a unique result file.
    nblines = 0
    shouldIcontinue = True
    with open(resultfile,"w",1) as fresults:
        with open(tmpdir+"/tmpresults") as results:
            while (shouldIcontinue):
                line = results.readline()
                fresults.write(line)
                fresults.flush()
                nblines+=1
                print(str(nblines)+" out of "+str(totSimulations))
                if (nblines == totSimulations):
                    shouldIcontinue = False
            print("Finished!")
            
main()
