#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import subprocess,sys
import scipy.stats,numpy
import time

def main():
    startt = time.time()
    print(startt)
    # Set which dir is the experiment in and create the FIFOs if necessary
    cwd = os.getcwd()
    tmpdir = cwd+"/tmp-statmc-exp"
    if (not os.path.exists(tmpdir+"/fifoin")):
        os.mkfifo(tmpdir+"/fifoin")
    if (not os.path.exists(tmpdir+"/fifoout")):
        os.mkfifo(tmpdir+"/fifoout")
    # The six following variables define the base state of the RNG Stream, they will be modified after each execution
    # 529786232 845593000 570807326 2505746817 4084951835 807889872
    # 2409254812 1209217041 2151262583 2863425416 3761735226 2184599142
    rngs = "767398895 1459543003 878188309 695383628 153745164 2283066547"
    rngs = rngs.split(" ")
    # Let's split that.
    rnga = rngs[0]
    rngb = rngs[1]
    rngc = rngs[2]
    rngd = rngs[3]
    rnge = rngs[4]
    rngf = rngs[5]
    # Should I continue ?
    continuesim = True
    totalsim = 0
    maxsim = -1
    simTimeout=0
    time_list = []
    # Now... let's execute our SimGrid model in parallel to something that will talk to it
    while (continuesim):
        if (totalsim > maxsim):
            continuesim = False
        if (len(time_list) > 0):
            resultatstmp = scipy.stats.t.interval(0.95, len(time_list)-1, loc=numpy.mean(time_list), scale=scipy.stats.sem(time_list))
            (mini,maxi) = resultatstmp
            if (maxi-mini < 1):
                print("Intervalle de confiance: [ "+str(mini)+","+str(maxi)+" ] après "+str(totalsim)+" simulations dont "+str(simTimeout)+" échouées")
                continuesim = False
            elif (totalsim % 50 == 0):
                print("Intervalle de confiance: [ "+str(mini)+","+str(maxi)+" ] après "+str(totalsim)+" simulations dont "+str(simTimeout)+" échouées")
        totalsim+=1
        newpid = os.fork()
        lasttime = 0.0
        if newpid==0:
            simulator = subprocess.Popen(["./s4u-bittorrent-statmc", tmpdir+"/platform.xml", tmpdir+"/deployment.xml", "0", tmpdir+"/mtstate0", tmpdir+"/fifoin", tmpdir+"/fifoout"], stdout=subprocess.PIPE)#, stderr=subprocess.PIPE)
#            simulator = subprocess.Popen(["./s4u-bittorrent-statmc", "tmp6-everyone-useSP/platform.xml", "tmp6-everyone-useSP/deployment.xml", rnga, rngb, rngc, rngd, rnge, rngf], stdout=subprocess.PIPE, stderr=sys.stderr.fileno())
            (output, err) = simulator.communicate()
            errorcode = simulator.wait()
            os._exit(0)
        else:
            with open(tmpdir+"/fifoin","w",1) as fifoin:
                with open(tmpdir+"/fifoout","r",1) as fifoout:
                    fifoin.write("TIMEOUT 50000\n")
                    fifoin.write("START\n")
                    fifoin.flush()
                    contreading = True
                    alreadyStopped = False
                    timeouted = False
                    while (contreading):
                        line = fifoout.readline()
                        if (line==""):
                            contreading=False
                            break
                        elements = line.strip()
                        elements = elements.split(" ")
                        print(elements)
                        if (elements[0] == "T"):
                            lasttime = float(elements[1])
                            if (elements[2] == "completed=2"):
                                if (elements[4] == "servers=2"):
                                    finishedat=float(elements[1])
                                    time_list.append(finishedat)
                                    fifoin.write("STOP\n")
                                    fifoin.flush()
                                    contreading=False
                            #if lasttime > 50000:
                            #    timeouted=True
                            #    fifoin.write("STOP\n")
                            #    fifoin.flush()
                            #   é contreading=False
                            if contreading:
                                fifoin.write("CONT\n")
                                fifoin.flush()
                            contreading=True
                        if (elements[0] == "RST"):
                            rnga = elements[1]
                            rngb = elements[2]
                            rngc = elements[3]
                            rngd = elements[4]
                            rnge = elements[5]
                            rngf = elements[6]
                        if (elements[0] == "END"):
                            contreading = False
                    endt=time.time()
                    print(endt)
                    print(endt-startt)
                if timeouted:
                    simTimeout+=1

main()
