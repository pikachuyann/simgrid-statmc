\documentclass[noauthor,nodate,english]{PointAvancement}

\title{SimgridStatMC}
\author{Yann Duplouy}

\usepackage{listings}
\lstdefinestyle{customcpp}{
	language=C++,
	frame=single,
	showstringspaces=false,
	numbers=left,
	numberstyle=\tiny,
	breaklines=true,
	basicstyle=\footnotesize\ttfamily,
	keywordstyle=\bfseries\color{purple!80!black},
	commentstyle=\itshape\color{black!70!white},
	tabsize=4,
}
\lstdefinestyle{custom}{
	frame=single,
	showstringspaces=false,
	numbers=left,
	numberstyle=\tiny,
	breaklines=true,
	basicstyle=\footnotesize\ttfamily,
	keywordstyle=\bfseries\color{purple!80!black},
	commentstyle=\itshape\color{black!70!white},
	tabsize=4,
}

\newcommand\FixMe[1]{\textcolor{red}{\textbf{FixMe:} #1}}
\begin{document}
\section{Prerequisites}
You will need \texttt{git}, \texttt{g++}, \texttt{bison}, \texttt{flex}, \texttt{libboost-dev} (1.67 or higher).

During the \emph{beta} phase of the SimgridStatMC tool, you will also need an account on framagit (\url{https://framagit.org/}) and ask access to the \texttt{pikachuyann/simgrid} and \texttt{pikachuyann/Simgrid-StatMC} repositories to Yann Duplouy.

\subsection{SimGrid with Stochastic Profiles}

\paragraph{Note.} SimGrid doesn't yet support Stochastic Profiles; it is a work in progress that will go through several Merge Requests. At the time of writing, a Merge Request for a unified RNG within SimGrid is in progress, which is a requirement for the Stochastic Profiles Merge Request.

\paragraph{Temporary solution.} The branch \texttt{custom\_sto\_simgrid} of the \texttt{pikachuyann/simgrid} repository is a slightly old (3.24.1) version of SimGrid, but with the support of stochastic profiles. You can either:
\begin{itemize}
	\item \texttt{git clone git@framagit.org:pikachuyann/simgrid.git} if you haven't got a SimGrid repository yet or want it to be completely separated to your SimGrid repository;
	\item \texttt{git remote add git@framagit.org:pikachuyann/simgrid.git}
\end{itemize}
In both cases, you must switch to the \texttt{StochasticProfiles} branch (\texttt{git checkout StochasticProfiles}) before compiling SimGrid.

In order to build SimGrid, create a build folder (like \texttt{build\_stoprofiles}), then once in it, use \texttt{cmake ..} then \texttt{make} and \texttt{sudo make install}.

\paragraph{What is a Stochastic Profile ?} Profiles are used in SimGrid to describe how a resource capacity changes during the simulation, by giving timestamps and current values. Stochastic Profiles are an extension of these Profiles, where you can describe timestamps and values using laws. Currently supported laws are \texttt{EXPONENTIAL}, \texttt{UNIFORM}, \texttt{NORMAL} and \texttt{DETERMINISTIC}.

\subsection{SimgridStatMC}

The SimgridStatMC tool comes currently only in the form of a git repository. It is available with:
\texttt{git clone git@framagit.org:pikachuyann/simgrid-statmc.git}.

Then you can compile it in the folder \texttt{SimgridStatMC} using \texttt{make}. For the moment, no configure file is provided so \texttt{g++}, \texttt{bison}, \texttt{flex} are required. Moreover, a high enough version of \texttt{libboost-dev} is required. Version 1.67 works on Debian.

\paragraph{Note (Grid5000).} Grid5k does not have by default bison nor flex, and those commands redirect to programs that cannot handle \emph{skeletons} for yacc files. You can use bison/flex on another computer to generate the required C++ sources for SimgridStatMC, and copy them to Grid5000.

The required files are, in both \texttt{LhaParser} and \texttt{Eval} directories, \texttt{position.hh}, \texttt{stack.hh}, \texttt{location.hh}, \texttt{y.tab.c}, \texttt{y.tab.h}. You will also need to copy \texttt{y.tab.h} to, respectively (in \texttt{LhaParser} and \texttt{Eval}), \texttt{Lha-parser.hh} and \texttt{Eval.hh}.

A zip file containing all those files can be compiled using \texttt{make getlexyacc}, or can be downloaded through \url{http://brindibou.pikachuyann.fr/simgrid-statmc-lexyacc.zip}. The zip should be unzipped in the \texttt{SimgridStatMC} folder.

Once that is done, use the \texttt{make nobison} option in the main \texttt{SimgridStatMC} folder.

\subsection{Additional files}
Files \texttt{include-statmc.cpp}, \texttt{include-statmc.hpp} and \texttt{donotinclude-statmc.hpp} are used to enhance the SimGrid simulator in order to be able to communicate with the SimgridStatMC tool. These doesn't require any form of compilation, but will need to be included and compiled with the scenario SimGrid simulator.

\section{Usage}
We assume that the simulator is developed using the S4U interface (\url{https://simgrid.org/doc/latest/app_s4u.html}). In this how to, we expect the Engine to have been initialised using:
\begin{lstlisting}[style=customcpp]
simgrid::s4u::Engine e(&argc, argv);
\end{lstlisting}

\subsection{Enhancing the simulator}
\paragraph{Summary.} The simulator will need to be able to communicate with the \texttt{SimgridStatMC} tool; for that it is currently required to have a specific format for the executable's arguments:

\texttt{exec <platform> <deployment> <default\_seed> <seed\_file> <fifoin> <fifoout>}

You also need to insert some line of codes into your (simulated) application for it to be able to communicate with \texttt{SimgridStatMC} and track data.

\paragraph{Headers and compilation.} You will need to include \texttt{include-statmc.hpp} to your headers, and compile \texttt{include-statmc.cpp} \emph{with} your simulator.

\paragraph{Communication.} To enable the communication with \texttt{SimgridStatMC}, you will need in your main to use
\begin{lstlisting}[style=customcpp]
simgrid::statmc::init(argv[5], argv[6]);
\end{lstlisting}
before starting the simulation (using \texttt{e.run()}). It is also required to initialise the RNG seed, which is done that way:
\begin{lstlisting}[style=customcpp]
simgrid::statmc::rng::set_mersenne_seed(atoi(argv[3]));
simgrid::statmc::use_mersenne_statefile(argv[4]);
struc stat st; #Requires including <sys/stat.h>
if (stat(argv[4], &st)==0) {
  simgrid::statmc::rng::read_mersenne_state(argv[4]);
}
\end{lstlisting}

\paragraph{Tracked variables.} Before starting the simulation (using \texttt{e.run()}), make sure to declare the variables you will want to track: 
\begin{lstlisting}[style=customcpp]
simgrid::statmc::createIntVariable(name, value);
simgrid::statmc::createFloatVariable(name, value);
\end{lstlisting}
Note that you can omit the value, and it will create a variable with the default value of \texttt{0}. On key points of your simulation, you can then update the value of these variables with:
\begin{lstlisting}[style=customcpp]
simgrid::statmc::updateIntVariable(name, value);
simgrid::statmc::updateFloatVariable(name, value);
\end{lstlisting}

\subsection{Describing the formula}
\label{sec:HASLdescription}
\paragraph{Text format (.lha).} SimgridStatMC uses the HASL (Hybrid Automata Stochastic Language) formalism, which is described in \cite{ballarini:hal-01221815}. Formulas are made of an automaton and an expression. They are described by a text file which has the following format:

\begin{lstlisting}[style=custom]
NbWatch=3;
NbVariables=2;
NbLocations=2;

VariablesList={t,ip};
WatchList={completed,inprogress,servers};
LocationsList={l0,l1};

time=AVG(Last(t));
tiprog=AVG(Last(ip));

InitialLocations={l0};
FinalLocations={l1};

Locations={
(l0, TRUE, (t:1,ip:inprogress/6));
(l1, (completed=servers)&(completed>0), (t:0,ip:0));
};

Edges={
((l0,l1), ALL, #, #);
};
\end{lstlisting}
First, you give the number of tracked variables (\texttt{NBWatch}) that are of interest for your formula, then the number of variables and locations of the automaton. You then give the name of those variables and tracked variables, making sure there is no overlap between tracked and automata variables. You then give the expressions you want to measure, and finally you describe the automaton.

\paragraph{Expressions.} A HASL expression is based on moments of a path random variable $Y$ and defined by the grammar :
$$ \begin{array}{rcl}
Z & := & c ~|~ P ~|~ \mathrm{AVG}(Y) ~|~ Z + Z ~|~ Z - Z ~|~ Z \times Z ~|~ Z / Z \\
Y & := & c ~|~ Y + Y ~|~ Y \times Y ~|~ Y / Y ~|~ \mathrm{Last}(y) ~|~ \mathrm{Min}(y) \\
  &    & \mathrm{Max}(y) ~|~ \mathrm{Int}(y) ~|~ \mathrm{Mean}(y) \\
y & := & c ~|~ x ~|~ y + y ~|~ y - y ~|~ y \times y ~|~ y / y
\end{array} $$
In this case, $x$ can be either a tracked or an automata variable. $P$ is the probability of accepted paths.

These expressions should appear after the lists of variables, tracked variables, and locations; they can be assigned a name through \texttt{name=expression} or directly given, following the grammar (use the same upper/lower characters than in the grammar). Use \texttt{*} for products, like \texttt{2*AVG(2+Last(3*completed))}.

\paragraph{InitialLocations, FinalLocations and RejectingLocations.} Respectively the set of possible initial locations (note that the automaton must be deterministic and that location invariants (see below) will force the choice of the initial location)), of the final locations (where the simulation successfully stops when reached), and of the rejecting locations (where the simulation stops and its results are rejected).

\paragraph{Locations.} A location is given in the following format: \\
\texttt{(LocationName, LocationInvariants, VariableRates)} \\
where \texttt{LocationName} is a location name previously given in \texttt{LocationList}, \texttt{LocationInvariants} a set of conditions that must be fulfilled while in the location. \texttt{VariableRates} are a list of \texttt{variable:rate}, where \texttt{variable} is an automata variable and rate an arithmetic expression constructed from automaton and watched variables.

\paragraph{Edges.} An edge is given in the following format: \\
\texttt{((sloc,floc), ALL, guards, updates} \\
where \texttt{sloc} is the name of the origin of the edge, and \texttt{floc} of the destination. \texttt{ALL} must stay unmodified (it is a placeholder for a list of events\footnote{HASL was designed for the tool Cosmos (\url{http://cosmos.lacl.fr/}) which is used as a baseline for SimgridStatMC. In Cosmos, events were the activation of transitions of the Petri net (or events of a discrete event stochastic process).}), \texttt{guards} is a list of guards on the automata variables (ie, \texttt{t >= 600}), and \texttt{updates} are a list of updates of automata variables (ie, \texttt{ip = 0}).

\subsection{Using SimgridStatMC and options}
The command line for SimgridStatMC follows: \\
\texttt{SimgridStatMC <options> pathtosimulator platform deployment formula}

Note: since the tool doesn't install in a folder in \texttt{PATH}, you may need to either use the relative or absolute path to the executable \emph{(which is the currently recommended solution)}, or add the folder to the PATH.

\subsubsection*{\texttt{-{}-level arg}}
This option sets the confidence level to \texttt{arg}. The default value is $0.99$.

\subsubsection*{\texttt{-{}-max-run arg}}
This option sets the maximum number of runs of the simulation to \texttt{arg}. The default value is $2000000$.

\subsubsection*{\texttt{-{}-min-run arg}}
This option sets the minimum number of runs of the simulation to \texttt{arg}. The default value is $100$

\subsubsection*{\texttt{-{}-nbjobs arg}}
This option sets the number of parallels threads to \texttt{arg}. The default value is $1$.

\subsubsection*{\texttt{-{}-relative}}
This options set the width to be relative to the mean; if the relative width is set to be $0.01$, then the width will be $0.01 \times \mathit{Mean}$

\subsubsection*{\texttt{-v, -{}-verbose arg}}
This option sets the verbosity level of the tool to \texttt{arg}. The default level is $2$, the current maximum level is $5$. This level limits the number of parallels threads to $1$.

\subsubsection*{\texttt{-{}-width arg}}
This option sets the width of the confidence interval to \texttt{arg}. The default value is $0.001$

\section{Example}
\subsection{BitTorrent}
This showcases a modified version of the BitTorrent example of SimGrid, so that actors can resume properly when their host recovers from a failure. In order to run the example, you have to build it (note that you need to have SimGrid installed). The script \texttt{build\_linux.sh} should successfully build it if you use \texttt{g++}, but the script easily be modifiable for another compiler if necessary.

Then, generate a scenario using \texttt{statmc\_exp\_generate.py}; a new folder will appear, \texttt{tmp-statmc-exp}. It contains a platform and deployment file for the simulated BitTorrent, alongside some stochastic traces (\texttt{state}$n$\texttt{.txt})

You can then use the script \texttt{useStatMC} to run the experiment. It runs the following command: \\
\texttt{../../SimgridStatMC/SimgridStatMC --njob 8 --width 0.005 --relative s4u-bittorrent-statmc tmp-statmc-exp/platform.xml tmp-statmc-exp/deployment.xml time.lha}

The HASL formula is described in \texttt{time.lha}, which was given as an example in section \ref{sec:HASLdescription}.

\bibliographystyle{plain}
\bibliography{documentation}
\end{document}
 
