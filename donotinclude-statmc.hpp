/* Copyright (c) 2019-2019. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef INCLUDE_STATMC_DNI_HPP
#define INCLUDE_STATMC_DNI_HPP

#include <fstream>
#include <map>

namespace simgrid {
namespace statmc {
bool activated;
std::ifstream pipe_in;
std::ofstream pipe_out;
bool didchange;
bool forcedstop;
bool mersenneExistsStateFile;
std::string mersenneStateFile;
double timeout;
std::map<std::string, float> floatVariables;
std::map<std::string, int> intVariables;
} // namespace statmc

namespace xbt {
namespace random {
std::mt19937 mt19937_gen;
}
}
} // namespace simgrid

#endif

