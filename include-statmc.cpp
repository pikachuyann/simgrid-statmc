/* Copyright (c) 2019-2019. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "include-statmc.hpp"
#include "donotinclude-statmc.hpp"
#include "xbt/random.hpp"
#include <csignal>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <simgrid/s4u.hpp>
#include <sstream>

XBT_LOG_NEW_CATEGORY(statmc, "Log channels of the statistical model-checking module");
XBT_LOG_NEW_DEFAULT_SUBCATEGORY(statmc_main, statmc, "Logging specific to the statistical model-checking module");

namespace simgrid {
namespace statmc {
void init(std::string inname, std::string outname)
{
  simgrid::statmc::activated = true;
  simgrid::statmc::didchange = false;
  simgrid::statmc::forcedstop = false;
  pipe_in.open(inname);
  pipe_out.open(outname);
  simgrid::s4u::Engine::on_time_advance.connect(&simgrid::statmc::send_variables_ontimeadvance);
  simgrid::s4u::Engine::on_time_advance.connect(simgrid::statmc::read_input_ontimeadvance);
  simgrid::s4u::Engine::on_simulation_end.connect(simgrid::statmc::send_variables_onend);
  simgrid::s4u::Engine::on_simulation_end.connect(simgrid::statmc::save_mersenne_state);
//  simgrid::s4u::on_simulation_start.connect(simgrid::statmc::read_input_start);
  simgrid::s4u::Engine::on_platform_creation.connect(simgrid::statmc::read_input_start);
  pipe_out << "READY" << std::endl;
  pipe_out.flush();
}
bool isActivated()
{
  return activated;
}

void read_input_start()
{
  read_input(0);
}
void read_input_ontimeadvance(double diff)
{
  if (simgrid::statmc::didchange || (simgrid::s4u::Engine::get_clock() > simgrid::statmc::timeout)) {
    read_input(1);
    simgrid::statmc::didchange = false;
  }
}
void read_input(int kind)
{
  bool blocked = true;
  pipe_out.flush();
  std::string currentline;
  while (blocked && getline(pipe_in, currentline)) {
    std::istringstream presplit(currentline);
    std::vector<std::string> splittedline(std::istream_iterator<std::string>{presplit},
                                          std::istream_iterator<std::string>());
    if (splittedline.size() == 0) {
      // What should we do of an empty line ?
    } else {
      if (kind == 0) {
        if (splittedline[0] == "START") {
          if (simgrid::statmc::timeout == 0) {
            simgrid::statmc::timeout = 2000000;
          }
          blocked = false;
        } else if (splittedline[0] == "TIMEOUT") {
          simgrid::statmc::timeout = std::stod(splittedline[1]);
        }
      } else if (kind == 1) { // When On Time Advance
        if (splittedline[0] == "CONT") {
          blocked = false;
        } else if (splittedline[0] == "STOP") {
          // Stop the simulation
          simgrid::statmc::forcedstop = true;
          simgrid::s4u::Engine::on_simulation_end();
          std::raise(SIGABRT);
          blocked = false;
        }
      }
    }
  }
}

void send_variables_ontimeadvance(double diff)
{
  if (simgrid::statmc::didchange || (simgrid::s4u::Engine::get_clock() > simgrid::statmc::timeout)) {
    send_variables();
  }
}

void send_variables_onend()
{
  if (not simgrid::statmc::forcedstop) {
    send_variables();
  }
}

void send_variables()
{
  using namespace simgrid::statmc;
  std::map<std::string, int>::iterator it;
  std::map<std::string, float>::iterator itf;
  pipe_out << "T " << std::to_string(simgrid::s4u::Engine::get_clock()) << " ";
  for (it = intVariables.begin(); it != intVariables.end(); it++) {
    pipe_out << it->first << "=" << it->second << " ";
  }
  for (itf = floatVariables.begin(); itf != floatVariables.end(); itf++) {
    pipe_out << itf->first << "=" << itf->second << " ";
  }

  pipe_out << std::endl;
  pipe_out.flush();
}
void use_mersenne_statefile(std::string filename)
{
  mersenneStateFile = filename;
  mersenneExistsStateFile = true;
}
void save_mersenne_state() {
  if (mersenneExistsStateFile) {
    simgrid::xbt::random::write_mersenne_state(mersenneStateFile);
  }
}

void createIntVariable(std::string name)
{
  createIntVariable(name, 0);
}
void createIntVariable(std::string name, int value)
{
  intVariables.insert(std::pair<std::string, int>(name, value));
  simgrid::statmc::didchange = true;
}
void updateIntVariable(std::string name, int value)
{
  intVariables[name] = value;
  simgrid::statmc::didchange = true;
}
int readIntVariable(std::string name)
{
  return intVariables[name];
}

void createFloatVariable(std::string name)
{
  createFloatVariable(name, 0);
}
void createFloatVariable(std::string name, float value)
{
  floatVariables.insert(std::pair<std::string, float>(name, value));
  simgrid::statmc::didchange = true;
}
void updateFloatVariable(std::string name, float value)
{
  floatVariables[name]= value;
  simgrid::statmc::didchange = true;
}
float readFloatVariable(std::string name)
{
  return floatVariables[name];
}


} // namespace statmc

/*
namespace xbt {
namespace random {
void set_mersenne_state(std::string filename)
{
  std::ifstream file(filename);
  file >> mt19937_gen;
}
void read_mersenne_state(std::string filename)
{
  std::ofstream file(filename);
  file << mt19937_gen;
}

} // namespace random
} // namespace xbt
*/

} // namespace simgrid
