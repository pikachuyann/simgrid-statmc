#ifndef __SGSMC_Experiment_
#define __SGSMC_Experiment_

#include <string>
#include <future>

#include "CosmosTools/LhaParser/Lha-Reader.hpp"

void run_simulator(std::string seed, std::string statefile, std::string fifoin, std::string fifoout);
void run_single_experiment(LhaType, int expid, std::ofstream& results);
void run_multiple_experiments(LhaType, std::future<void> futureObject, int expid);
void run_experiment(LhaType);

#endif