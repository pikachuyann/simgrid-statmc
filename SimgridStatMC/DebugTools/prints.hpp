#include "../CosmosTools/LhaParser/Lha-Reader.hpp"

std::ostream& operator<<(std::ostream& os, const LhaType& mylha);
std::ostream& operator<<(std::ostream& os, const MarkingFormula& mf);
std::ostream& operator<<(std::ostream& os, const ComparisonExpression& mf);
std::ostream& operator<<(std::ostream& os, const AlgebraicExpression& ae);
std::ostream& operator<<(std::ostream& os, const LHAFunc& lf);