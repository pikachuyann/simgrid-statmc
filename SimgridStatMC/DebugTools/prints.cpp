#include "prints.hpp"

std::ostream& operator<<(std::ostream& os, const MarkingFormula& mf) {
	switch (mf.TypeOp) {
		case MF_EQUAL:
			os << "(" << *mf.left << "=" << *mf.right << ")";
			break;
		case MF_PLUS:
			os << "(" << *mf.left << "+" << *mf.right << ")";
			break;
		case MF_MINUS:
			os << "(" << *mf.left << "-" << *mf.right << ")";
			break;
		case MF_MULT:
			os << "(" << *mf.left << "*" << *mf.right << ")";
			break;
		case MF_DIV:
			os << "(" << *mf.left << "/" << *mf.right << ")";
			break;
		case MF_ASSIGN:
			os << "(" << *mf.left << ":=" << *mf.right << ")";
			break;
		case MF_NEG:
			os << "-" << "(" << *mf.left << ")";
			break;
		case MF_POW:
			os << *mf.left << "^" << *mf.right << ")";
			break;
		case MF_FLOOR:
			os << "floor(" << *mf.left << ")";
			break;
		case MF_MIN:
			os << "min(" << *mf.left << "," << *mf.right << ")";
			break;
		case MF_MAX:
			os << "max(" << *mf.left << "," << *mf.right << ")";
			break;
		case MF_MOD:
			os << "(" << *mf.left << "%" << *mf.right << ")";
			break;
		case MF_REAL:
			os << mf.realval;
			break;
		case MF_INT:
			os << mf.intval;
			break;
		case HYBRIDVAR:
			os << "Hybrid[" << mf.varname << "]";
			break;
		case WATCHEDVAR:
			os << "Watched[" << mf.varname << "]";
			break;
		case LHAVAR:
			os << "Lha[" << mf.varname << "]";
			break;
		case MF_EMPTY:
			os << "empty";
			break;
		default:
			os << "todo MarkingFormula (" << mf.TypeOp << ")";
	}
}

std::ostream& operator<<(std::ostream& os, const ComparisonExpression& ce) {
	switch (ce.TypeOp) {
		case CE_LEQ:
			os << "(" << *ce.left << " <= " << *ce.right << ")";
			break;
		case CE_GEQ:
			os << "(" << *ce.left << " => " << *ce.right << ")";
			break;
		case CE_LL:
			os << "(" << *ce.left << " < " << *ce.right << ")";
			break;
		case CE_GG:
			os << "(" << *ce.left << " > " << *ce.right << ")";
			break;
		case CE_EQ:
			os << "(" << *ce.left << " == " << *ce.right << ")";
			break;
		case CE_TRUE:
			os << "true";
			break;
		case CE_FALSE:
			os << "true";
			break;
		case CE_NOT:
			os << "not(" << *ce.left_ce << ")";
			break;
		case CE_AND:
			os << "(" << *ce.left_ce << " && " << *ce.right_ce << ")";
			break;
		case CE_OR:
			os << "(" << *ce.left_ce << " || " << *ce.right_ce << ")";
			break;
		case CE_EMPTY:
			os << "empty";
			break;
		default:
			os << "todo ComparisonExpression (" << ce.TypeOp << ")";
	}
}

std::ostream& operator<<(std::ostream& os, const LHAFunc& lh)
{
	switch (lh.TypeOp) {
		case LF_MEAN:
			os << "Mean(" << *lh.left << ")";
			break;
		case LF_MIN:
			os << "Min(" << *lh.left << ")";
			break;
		case LF_MAX:
			os << "Max(" << *lh.left << ")";
			break;
		case LF_INT:
			os << "Int(" << *lh.left << ")";
			break;
		case LF_LAST:
			os << "Last(" << *lh.left << ")";
			break;
		default:
			os << "todo_print_LHAFunc";
	}
}

std::ostream& operator<<(std::ostream& os, const AlgebraicExpression& ae) {
	switch (ae.TypeOp) {
		case AE_LhaFunc:
			os << "(LhaFunc[" << ae.LhaFunc << "])";
			break;
		case AE_PLUS:
			os << "(" << *ae.left << "+" << *ae.right << ")";
			break;
		case AE_MINUS:
			os << "(" << *ae.left << "-" << *ae.right << ")";
			break;
		case AE_MULT:
			os << "(" << *ae.left << "*" << *ae.right << ")";
			break;
		case AE_DIV:
			os << "(" << *ae.left << "/" << *ae.right << ")";
			break;
		case AE_NEG:
			os << "-" << "(" << *ae.left << ")";
			break;
		case AE_POW:
			os << *ae.left << "^" << *ae.right << ")";
			break;
		case AE_FLOOR:
			os << "floor(" << *ae.left << ")";
			break;
		case AE_MIN:
			os << "min(" << *ae.left << "," << *ae.right << ")";
			break;
		case AE_MAX:
			os << "max(" << *ae.left << "," << *ae.right << ")";
			break;
		default:
			os << "todo";
	}
}

std::ostream& operator<<(std::ostream& os, const LhaType& lha) {
	os << "Printing the parsed Lha:" << std::endl;
	os << "----------------------------------------------------------" << std::endl;
	os << "Variables:" << std::endl;
	for (size_t i=0; i < lha.Vars.label.size(); i++) {
		os << "  | Variable " << i << ": " << lha.Vars.label[i] << " (type: " << lha.Vars.type[i] << ", initValue: " << lha.Vars.initialValue[i] << ")" << std::endl;
	}
	os << std::endl;
	os << "Locations:" << std::endl;
	for (size_t i=0; i < lha.LocLabel.size(); i++) {
		os << "  | Location " << i << ": " << lha.LocLabel[i] << " and FuncLocProperty: " << *lha.FuncLocProperty[i] << std::endl;
		os << "  |  |- Synchronizing edges (out): ";
		for (int edge : lha.Out_S_Edges[i]) {
			os << edge << " ";
		}
		os << std::endl << "  |  |- Autonomous edges (out): ";
		for (int edge : lha.Out_A_Edges[i]) {
			os << edge << " ";
		}
		os << std::endl;
	}
	os << std::endl;
	os << "Initial Locations: ";
	for (auto loc : lha.InitLoc) {
		os << loc << " ";
	}
	os << std::endl;
	os << "Final Locations: ";
	for (auto loc : lha.FinalLoc) {
		os << loc << " ";
	}
	os << std::endl;
	os << std::endl;
	os << "Func flow" << std::endl;
	for (size_t i=0; i < lha.FuncFlow.size();i++) {
		for (size_t j=0;j < lha.FuncFlow[i].size();j++) {
			if (lha.FuncFlow[i][j] != NULL) {
				os << "  | (" << i << "," << j << "): " << *lha.FuncFlow[i][j] << std::endl;
			} else {
				os << "  | (" << i << "," << j << "): NULLPOINTER" << std::endl;
			}
		}
	}
	os << std::endl;
	os << "Lha Edges:" << std::endl;
	for (size_t i=0 ; i < lha.Edge.size();i++) {
		LhaEdge current = lha.Edge[i];
		os << "Edge " << i << ": (index) " << current.Index << " from " << current.Source << " to " << current.Target << std::endl;
		os << "  | Actions: " << std::endl;
		for (string const& act : lha.EdgeActions[i]) {
			os << "  |  |- " << act << std::endl;
		}
		os << "  | (unTimeEdge)Constraint: " << lha.unTimeEdgeConstraints[i] << std::endl;
		// os << "  | ConstraintsCoeffs: " << lha.ConstraintsCoeffs[current.Source][current.Target][0] << std::endl;
		os << "  |__________________________________________________________" << std::endl;
	}
	os << std::endl;
	os << "Constraints coeff" << std::endl;
	for (size_t i=0; i < lha.ConstraintsCoeffs.size();i++) {
		for (size_t j=0;j < lha.ConstraintsCoeffs[i].size(); j++) {
			for (size_t k=0;k < lha.ConstraintsCoeffs[i][j].size(); k++) {
				os << "  | (" << i << "," << j << "," << k << "): " << lha.ConstraintsCoeffs[i][j][k] << std::endl;
			}
		}
	}
	os << std::endl;
	os << "Constraints Relop" << std::endl;
	for (size_t i=0; i < lha.ConstraintsRelOp.size();i++) {
		for (size_t j=0;j < lha.ConstraintsRelOp[i].size(); j++) {
			os << "  | (" << i << "," << j << "): " << lha.ConstraintsRelOp[i][j] << std::endl;
		}
	}
	os << std::endl;
	os << "Constraints Constants" << std::endl;
	for (size_t i=0;i < lha.ConstraintsConstants.size();i++) {
		for (size_t j=0; j < lha.ConstraintsConstants[i].size(); j++) {
			if (lha.ConstraintsConstants[i][j] != NULL) {
				os << "  | (" << i << "," << j << "): " << *lha.ConstraintsConstants[i][j] << std::endl;
			} else {
				os << "  | (" << i << "," << j << "): NULLPOINTER" << std::endl;
			}
		}
	}
	os << std::endl;
	os << "Func Edge Updates" << std::endl;
	for (size_t i=0;i < lha.FuncEdgeUpdates.size();i++) {
		for (size_t j=0;j < lha.FuncEdgeUpdates[i].size();j++) {
			if (lha.FuncEdgeUpdates[i][j] != NULL) {
				os << "  | (" << i << "," << j << "): " << *lha.FuncEdgeUpdates[i][j] << std::endl;
			} else {
				os << "  | (" << i << "," << j << "): NULLPOINTER" << std::endl;
			}
		}
	}

	os << std::endl;
	os << "Lha Functions" << std::endl;
	for (map<LHAFunc*, int>::const_iterator it = lha.LhaFunction.begin(); it != lha.LhaFunction.end(); ++it) {
		if (it->first != NULL) {
			std::cout << "  | " << *it->first << " : " << it->second << std::endl;
		} else {
			std::cout << "  | NULLPOINTER : " << it->second << std::endl;
		}
	}
	os << std::endl;
	os << "Algebraic" << std::endl;
	for (size_t i=0;i < lha.Algebraic.size();i++) {
		if (lha.Algebraic[i] != NULL) {
			os << "  | Algebraic " << i << " : " << *lha.Algebraic[i] << std::endl;
		} else {
			os << "  | Algebraic " << i << " : NULLPOINTER" << std::endl;
		}
	}
	os << std::endl;
}