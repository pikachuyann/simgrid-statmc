#include <cstdio>

#include "experiment.hpp"
#include "CosmosTools/parameters.hpp"
#include "CosmosTools/LhaParser/Lha-Reader.hpp"

parameters P;

int main(int argc, char** argv) {
	std::printf("This is currently a prototype.\n");

	P.parseCommandLine(argc, argv);
	Lha_Reader LhaRead(P);
	int returnvalue = LhaRead.parse_file(P);
	
	if (returnvalue) {
		std::cerr << "LHA cannot be created." << std::endl;
	} else {	
		if (LhaRead.MyLha.isDeterministic) {
			if (P.verbose > 4 && P.Njob > 1) {
				std::cerr << "A high verbose level has been chosen and the number of simultaneous threads cannot be higher than 1. Running with one thread." << std::endl;
				P.Njob=1;
			}
			run_experiment(LhaRead.MyLha);
		} else {
			std::cerr << "Cannot deal with a nondeterministic LHA yet, sorry." << std::endl;
		}
	}
}
