#include <thread>
#include <boost/process.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <future>
#include <vector>
#include <limits>
#include <map>

#include "experiment.hpp"
#include "CosmosTools/parameters.hpp"
#include "CosmosTools/BatchR.hpp"
#include "CosmosTools/result.hpp"
#include "CosmosTools/LhaParser/Lha-Reader.hpp"
#include "CosmosTools/LHA.hpp"

void run_simulator(std::string seed, std::string statefile, std::string fifoin, std::string fifoout) { //std::string simulator, std::string platform, std::string deployment, std::string seed, std::string statefile, std::string fifoin, std::string fifoout) {
	try {
		boost::process::child thesim(P.programName, P.platformFile, P.deploymentFile, seed, statefile, fifoin, fifoout, boost::process::std_out > boost::process::null, boost::process::std_err > boost::process::null);
		thesim.wait();
	} catch (boost::process::process_error& e) {
		if (e.code().value() == 10) {
			// That is probably the standard output where the SimGrid simulator finishes abruptly because this program asked so with a STOP instruction.
			// Therefore, we pass this silently.
		} else {
			std::cout << "Caught process error with code " << e.code().value() << " meaning " << e.what() << std::endl;
		}
	}
}

void run_single_experiment(LhaType lha, int expid, std::ofstream& results) {
	// An experiment is made of two threads:
	// a) One thread running the SimGrid simulator
	// b) Another thread talking, through named pipes, with the simulator

	std::thread t1(run_simulator, std::to_string(expid), P.tmpPath+"/mtstate"+std::to_string(expid), P.tmpPath+"/fifoin"+std::to_string(expid), P.tmpPath+"/fifoout"+std::to_string(expid));

	// The pipes are named with respect to the simulator inputs/outputs.
	std::ofstream pipe_in;
	std::ifstream pipe_out;
	pipe_in.open((P.tmpPath+"/fifoin"+std::to_string(expid)).c_str());
	pipe_out.open((P.tmpPath+"/fifoout"+std::to_string(expid)).c_str());
	
	std::string currentline;
	bool blocked;
	blocked = true;
	double current_time;
	size_t simstep=0;
	
	std::map<std::string, double> watchedvariables;
	std::map<std::string, double> previouswatchedvariables;
	LHA TheLha(lha);
	
	double lha_nexttime = std::numeric_limits<double>::max();
	double lha_currenttime = 0;
	double lha_previoustime = 0;
	
	// Let's wake up the SimGrid simulator
	pipe_in << "START " << std::endl;
	pipe_in.flush();
	// ToDo: a possible TIMEOUT instruction
	
	// Now, let's communicate with the SimGrid simulator
	while (blocked && getline(pipe_out, currentline)) {
		std::vector<std::string> splittedline;
		boost::split(splittedline, currentline, [](char c){return c == ' ';});
		if (P.verbose > 4) { std::cerr << std::endl << "From SimGrid: " << currentline << std::endl; }
		
		if (splittedline.size() == 0) {
		} else {
			if (splittedline[0]=="T") {
				current_time = std::stod(splittedline[1]);
				if (lha_nexttime > current_time) { lha_nexttime = current_time; } // Force the LHA to check whether it can change state, or force a computation of its variable.
				
				// Step 1: Get the list of variables that are watched/monitored through the stochastic simulator
				previouswatchedvariables = watchedvariables;
				watchedvariables.clear();
				for (int i=2;i < splittedline.size();i++) {
					if (splittedline[i]=="") { } else {
						std::vector<std::string> splittedcontent;
						boost::split(splittedcontent, splittedline[i], [](char c){return c == '=';});
						if (splittedcontent.size() != 2) {
							std::cerr << "Malformed watched variable: " << splittedline[i] << std::endl;
							std::cerr << "Complete line was: " << currentline << "with " << splittedline.size() << " elements " << std::endl;
						} else {
							if (!watchedvariables.insert(std::make_pair(splittedcontent[0], std::stod(splittedcontent[1]))).second ) {
								std::cerr << "Watched variable seen twice at least: " << splittedcontent[0] << std::endl;
								std::cerr << "Complete line was: " << currentline << std::endl;
							}
						}
					}
				}
				// For the first step of the simulation, we consider that the watched variables are initialized and for the LHA logic… (should improve this comment)
				if (simstep == 0) {
					previouswatchedvariables=watchedvariables; simstep++;
					TheLha.setInitLocation(previouswatchedvariables);
				}
				
				if (P.verbose > 4) {
					std::cerr << "WatchedVariables: ";
					for (std::pair<std::string, double> element : watchedvariables) {
						std::cerr << element.first << "=" << element.second << " ";
					}
					std::cerr << std::endl << "PreviousWatchedVariables: ";
					for (std::pair<std::string, double> element : previouswatchedvariables) {
						std::cerr << element.first << "=" << element.second << " ";
					}
					std::cerr << std::endl;
				}
				
				// Step 2: Compute the LHA next step(s)
				while (TheLha.CurrentTime < current_time && !TheLha.isFinal()) {
					AutEdge AE = TheLha.GetEnabled_A_Edges(previouswatchedvariables);
					if (AE.Index > -1 && AE.FiringTime < current_time) {
						TheLha.updateLHA( AE.FiringTime - TheLha.CurrentTime, previouswatchedvariables );
						TheLha.fireAutonomous( AE.Index, previouswatchedvariables);
					} else {
						TheLha.updateLHA( current_time - TheLha.CurrentTime, previouswatchedvariables);
//						TheLha.CurrentTime = current_time;
						AE = TheLha.GetEnabled_A_Edges(watchedvariables);
						if (AE.Index > -1 && AE.FiringTime == current_time) {
							TheLha.fireAutonomous( AE.Index, watchedvariables );
						}
					}
					/*
					// Step 2.1: Make time pass
					lha_previoustime = lha_currenttime;
					lha_currenttime = lha_nexttime;
					if (lha_nexttime < current_time) { lha_nexttime = current_time; }
					else { lha_nexttime = std::numeric_limits<double>::max(); } // This will be the maximum value of lha_nexttime, it is forced to current_time if we're not already there
					
					TheLha.updateLHA(lha_currenttime - lha_previoustime,previouswatchedvariables);
					*/
				}
				
				if (P.verbose>4) { std:cerr << "Current value of Algebraics: " << TheLha.GetAlgebraics() << std::endl; }
				
				// Step n: Check whether the simulation should complete.
				if (TheLha.isFinal()) { /* watchedvariables["completed"] == watchedvariables["servers"] && watchedvariables["servers"] == 8) { */
					// Step n.1: Compute and send the Algebraics
					if (P.verbose > 4) { std::cerr << "RESULT = Accepted" << std::endl; }
					results << "ACCEPT " << TheLha.GetAlgebraics() << std::endl;
					results.flush();
					// Step n.2: Stop the SimGrid simulator
					pipe_in << "STOP" << std::endl;					
					blocked=false;
				} else if (!TheLha.isPossible(watchedvariables) || TheLha.isRejecting()) {
					if (P.verbose > 4) { std::cerr << "RESULT = Refused" << std::endl; }
					results << "REFUSE " << TheLha.GetAlgebraics() << std::endl;
					results.flush();
					pipe_in << "STOP" << std::endl;
					blocked=false;
				} else {
					if (P.verbose > 4) { std::cerr << "Continue simulation" << std::endl; }
					pipe_in << "CONT" << std::endl;
					blocked=true;
				}
			}
		}
		pipe_in.flush();
	}
	t1.join();
}

void run_multiple_experiments(LhaType lha, std::future<void> futureObject, int expid) {
	mkfifo( (P.tmpPath+"/fifoin"+std::to_string(expid)).c_str(), 0644 );
	mkfifo( (P.tmpPath+"/fifoout"+std::to_string(expid)).c_str(), 0644 );
	
	std::ofstream results;
	results.open((P.tmpPath+"/fiforesults").c_str());

	// A complicated way to say "if you haven't got the signal yet, continue."
	while (futureObject.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout) {
		run_single_experiment(lha, expid, results);
	}
}

void run_experiment(LhaType lha)
{
	// Create the necessary P.tmpPath:
	mkdir( P.tmpPath.c_str(), 0755);
	
	// Create the necessary FIFO for the results:
	mkfifo( (P.tmpPath+"/fiforesults").c_str(), 0644 );

	// Create the nbthread threads running experiments :
	std::vector<std::thread> threadVec;
	std::vector<std::promise<void>> stopSignals;
	for (int i=0;i < P.Njob;i++) {
		// The three following lines are used to make sure the experiments are stopped properly whenever we have enough results:
		std::promise<void> stopSignal;
		stopSignals.push_back(std::move(stopSignal));
		std::future<void> futureObject = stopSignals[i].get_future();
		// Actually create the thread:
		std::thread ttmp(run_multiple_experiments, lha, std::move(futureObject), i);
		threadVec.push_back(std::move(ttmp));
	}

	// Read results:
	bool blocked = true;
	std::string currentline;
	std::vector<std::string> splittedline;
	std::ifstream results;

	
	/* HaslFormulasTop hft(0);
	
	P.HaslFormulas.push_back(new HaslFormulasTop(0));
	P.HaslFormulasname.push_back("Avg");
	P.nbAlgebraic=1; */
	
	P.HaslFormulas = lha.HASLtop;
	P.HaslFormulasname = lha.HASLname;
	P.nbAlgebraic = lha.Algebraic.size();
	
	result resultats;
	ConfInt confint;
	unsigned long int nbruns = 0;
	results.open( (P.tmpPath+"/fiforesults").c_str() );
	while (blocked && getline(results, currentline)) {
		boost::split(splittedline, currentline, [](char c){return c == ' ';});
		SimOutput so;
		if (P.verbose > 4) {
			std::cerr << "Received: " << currentline << std::endl << std::endl;
		}
		if (splittedline[0]=="ACCEPT") { so.accept=1; } else { so.accept=0; }
		for (int i = 1;i < splittedline.size();i++) {
			so.quantR.push_back(std::stod(splittedline[i]));
		}
		resultats.addSim(so);
		//confint = hft.eval(resultats.MeanM2);
		if (P.verbose < 5) {
			resultats.printProgress();
		}
		nbruns += 1;
		blocked=resultats.continueSim() || (nbruns <= P.MinRuns);
	}

	resultats.stopclock();
	resultats.print(std::cout);
	resultats.printResultFile("results.txt");
	
	// Join threads when finished:
	for (int j=0;j < P.Njob;j++) {
		stopSignals[j].set_value();
		threadVec[j].join();
	}
	
}