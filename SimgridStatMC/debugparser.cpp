#include <cstdio>

#include "experiment.hpp"
#include "CosmosTools/parameters.hpp"
#include "CosmosTools/LhaParser/Lha-Reader.hpp"
#include "DebugTools/prints.hpp"

parameters P;

int main(int argc, char** argv) {
	P.parseCommandLine(argc, argv);
	Lha_Reader LhaRead(P);
	int returnlha = LhaRead.parse_file(P);
	
	if (returnlha) {
		std::cerr << "LHA cannot be created." << std::endl;
	} else {
		std::cout << LhaRead.MyLha;
	}
}