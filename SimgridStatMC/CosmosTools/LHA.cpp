#include "LHA.hpp"
#include "LhaParser/Lha-Reader.hpp"

#include <map>
#include <string>
#include <limits>
#include <string>

LHA::LHA(LhaType lt):NbLoc(lt.NbLoc), NbTrans(lt.TransitionIndex.size()), NbVar(lt.NbVar), FinalLoc(lt.NbLoc,false), RejectingLoc(lt.NbLoc,false), CurrentTime(0) {
	lha = lt;
	
	for (int i=0;i < lha.Vars.label.size(); i++) {
		lha_variables.insert( std::make_pair(lha.Vars.label[i], lha.Vars.initialValue[i]) );
	}
	
	for (map<LHAFunc*, int>::const_iterator it = lha.LhaFunction.begin(); it != lha.LhaFunction.end(); ++it) {
		// This first iteration is to create the vector lha_funcvalue to the right size;
		LhaFunc.push_back(0);
	}
	for (map<LHAFunc*, int>::const_iterator it = lha.LhaFunction.begin(); it != lha.LhaFunction.end(); ++it) {
		// This second iteration is to fill the lha_funcvalue vector with the right starting values.
		if (it->first == NULL) {
			LhaFunc[it->second] = 0;
		} else {
			LhaFunc[it->second] = it->first->startValue();
		}
	}
	
	for (const auto &l : lha.FinalLoc) {
		FinalLoc[l] = true;
	}
	for (const auto &l : lha.RejectingLoc) {
		RejectingLoc[l] = true;
	}
}

void LHA::DoElapsedTimeUpdate(double delta, std::map<std::string, double> watchedvars) {
	for (int i=0;i < lha.Vars.label.size();i++) {
		lha_variables[lha.Vars.label[i]] += delta * GetFlow(i, watchedvars);
	}
}

double LHA::GetFlow(int v, std::map<std::string, double> watchedvars) {
	if (lha.FuncFlow[CurrentLocation][v] != NULL) {
		if (lha.FuncFlow[CurrentLocation][v]->TypeOp == MF_EMPTY) {
			return 0;
		} else {
			return (lha.FuncFlow[CurrentLocation][v])->eval(watchedvars,lha_variables);
		}
	} else {
		return 0;
	}
}

bool LHA::CheckLocation(int loc, std::map<std::string, double> watchedvars) {
	return (lha.FuncLocProperty[loc])->eval(watchedvars,lha_variables);
}

bool LHA::CheckEdgeConstraints(int e, size_t ptt, std::map<std::string, double> watchedvars) {
	// Q: What is the role of ptt ? #ToDo
	// ToDo: unTimeEdgeConstraints may yet have to be transformed into a ComparisonExpression or something similar?
	if ( (lha.ConstraintsRelOp[e].size() > 0 && lha.EdgeActions[e].size() > 0) || lha.unTimeEdgeConstraints[e] != "true") {
		if (lha.ConstraintsRelOp[e].size() > 0 && lha.EdgeActions[e].size() > 0) {
			double accumulator;
			// ToDo: ConstraintsCoeffs may directly be parsed into a double ?
			for (size_t c = 0;c < lha.ConstraintsRelOp[e].size(); c++) {
				size_t k = 0;
				for (size_t v = 0; v < lha.NbVar; v++) {
					k++;
					if (lha.ConstraintsCoeffs[e][c][v] != "") {
						accumulator += std::stod(lha.ConstraintsCoeffs[e][c][v]) * (lha_variables[lha.Vars.label[v]]);
					}
				}
				double comparedto = (lha.ConstraintsConstants[e][c])->eval(watchedvars, lha_variables);
				
				std::string op = lha.ConstraintsRelOp[e][c];
				if (k > 0) {
					if (op == "<=" && accumulator > comparedto) { return false; }
					if (op == "==" && accumulator != comparedto) { return false; }
					if (op == ">=" && accumulator < comparedto) { return false; }
					if (op != "<=" && op != "==" && op != "=>") { std::cerr << "The operator " << op << " is not yet implemented for CheckEdgeConstraints." << std::endl; }
				}
			}
		}
		
		if (lha.unTimeEdgeConstraints[e] == "true") { return true; }
		else { std::cerr << "UnTimeEdgeConstraints (" << lha.unTimeEdgeConstraints[e] << ") is different from true." << std::endl; }
	}
	
	return true;
}

t_interval LHA::GetEdgeEnablingTime(int e, std::map<std::string, double> watchedvars) {	
	if (lha.ConstraintsRelOp[e].size() > 0 && lha.EdgeActions[e].size() < 1) {
		t_interval EmptyInterval;
		EmptyInterval.first=0;
		EmptyInterval.second=-1;
		t_interval EnablingT;
		EnablingT.first=CurrentTime;
		EnablingT.second=std::numeric_limits<double>::max();
		double SumAF;
		double SumAX;
		
		for (size_t c=0;c < lha.ConstraintsRelOp[e].size(); c++) {
			SumAF=0; SumAX=0;
			
			for (size_t v=0;v < lha.NbVar;v++) {
				if (lha.ConstraintsCoeffs[e][c][v] != "") {
					SumAF+=std::stod(lha.ConstraintsCoeffs[e][c][v]) * GetFlow(v, watchedvars);
					SumAX+=std::stod(lha.ConstraintsCoeffs[e][c][v]) * (lha_variables[lha.Vars.label[v]]);
				}
			}
			double comparedto = (lha.ConstraintsConstants[e][c])->eval(watchedvars, lha_variables);
			std::string op = lha.ConstraintsRelOp[e][c];
			
			if (SumAF==0) {
				if (op == "<=" && SumAX > comparedto) { return EmptyInterval; }
				if (op == "==" && SumAX != comparedto) { return EmptyInterval; }
				if (op == ">=" && SumAX < comparedto) { return EmptyInterval; }
				if (op != "<=" && op != "==" && op != ">=") { std::cerr << "The operator " << op << " is not yet implemented for GetEdgeEnablingTime, case SumAF=0." << std::endl; }
			} else {
				double t = CurrentTime + (comparedto - SumAX)/SumAF;
				if (op == "==") {
					if (t>=EnablingT.first & t<=EnablingT.second) { EnablingT.first=t; EnablingT.second=t; }
					else { return EmptyInterval; }
				} else {
					if (SumAF>0) {
						if (op == "<=") {
							if (EnablingT.second>t) { EnablingT.second=t; }
							if (EnablingT.second < EnablingT.first) { return EmptyInterval; }
						}
						else if (op == ">=") {
							if (EnablingT.first<t) { EnablingT.first=t; }
							if (EnablingT.second < EnablingT.first) { return EmptyInterval; }
						}
						else {
							std::cerr << "The operator " << op << " is not yet implemented for GetEdgeEnablingTime, case SumAF>0." << std::endl;
						}
					}
					else {
						if (op == ">=") {
							if (EnablingT.second>t) { EnablingT.second=t; }
							if (EnablingT.second < EnablingT.first) { return EmptyInterval; }
						}
						else if (op == "<=") {
							if (EnablingT.first<t) { EnablingT.first=t; }
							if (EnablingT.second < EnablingT.first) { return EmptyInterval; }
						}
						else {
							std::cerr << "The operator " << op << " is not yet implemented for GetEdgeEnablingTime, case SumAF<0." << std::endl;
						}
					}
				}
			}
		}
		return EnablingT;
	}
	else {
		t_interval EnablingT;
		EnablingT.first=CurrentTime;
		EnablingT.second=std::numeric_limits<double>::max();
		return EnablingT;
	}
}


void LHA::UpdateLhaFunc(double DeltaT, std::map<std::string, double> watchedvars) {
	for (map<LHAFunc*, int>::const_iterator it = lha.LhaFunction.begin(); it != lha.LhaFunction.end(); ++it) {
		if (it->first == NULL) { }
		else {
			LhaFunc[it->second] = it->first->eval(LhaFunc[it->second], DeltaT, CurrentTime, watchedvars, lha_variables);
		}
	}
	if (P.verbose > 4) {
		std::cerr << "LhaFuncs:";
		for (size_t i = 0;i < LhaFunc.size();i++) {
			std::cerr << " " << LhaFunc[i];
		}
		std::cerr << std::endl;
	}
}

void LHA::DoEdgeUpdates(int e, std::map<std::string, double> watchedvars) {
	size_t k = 0;
	std::map<std::string, double> oldvars = lha_variables; // The update of variables may require using old values
	//for (size_t v = 0; v < lha.NbVar; v++) { if (lha.FuncEdgeUpdates[e][v] != NULL) { k++; } }
	// // This was for the case of distinguishing whether we only had one variable to update
	
	for (size_t v = 0;v < lha.NbVar; v++) {
		if (lha.FuncEdgeUpdates[e][v] != NULL) {
			lha_variables[lha.Vars.label[v]] = lha.FuncEdgeUpdates[e][v]->eval(watchedvars, oldvars);
		}
	}
}

// LHA_orig functions
void LHA::updateLHA(double DeltaT, std::map<std::string, double> watchedvars) {
	if (P.verbose > 4) { std::cerr << "Updating LHA with DeltaT=" << DeltaT << std::endl; }
	DoElapsedTimeUpdate(DeltaT, watchedvars);
	UpdateLhaFunc(DeltaT, watchedvars);
	CurrentTime += DeltaT;
}

AutEdge LHA::GetEnabled_A_Edges(std::map<std::string, double> watchedvars) {
	AutEdge Ed;
	Ed.Index = -1;
	Ed.FiringTime = std::numeric_limits<double>::max();
	for (auto it: lha.Out_A_Edges[CurrentLocation]) {
		if (CheckLocation(lha.Edge[it].Target, watchedvars) && CheckEdgeConstraints(it, 0, watchedvars)) {
			t_interval I = GetEdgeEnablingTime(it, watchedvars);
			if (I.first <= I.second) {
				if (I.first <= Ed.FiringTime) {
					Ed.Index = it;
					Ed.FiringTime = I.first;
				}
			}
		}
	}
	
	return Ed;
}

void LHA::fireAutonomous(int EdgeIndex, std::map<std::string, double> watchedvars) {
	DoEdgeUpdates(EdgeIndex, watchedvars);
	CurrentLocation = lha.Edge[EdgeIndex].Target;
}

void LHA::fireLHA(int EdgeIndex, std::map<std::string, double> watchedvars) {
	DoEdgeUpdates(EdgeIndex, watchedvars);
	CurrentLocation = lha.Edge[EdgeIndex].Target;
}

void LHA::setInitLocation(std::map<std::string, double> watchedvars) {
	for (const auto &l : lha.InitLoc) {
		if (CheckLocation(l, watchedvars)) {
			CurrentLocation = l;
			if (P.verbose > 4) { std::cerr << "LHA location set to " << CurrentLocation << " its initial location." << std::endl; }
			return;
		}
	}
}

// New functions
std::string LHA::GetAlgebraics() {
	std::ostringstream os;
	size_t k=0;
	for (int i=0;i < lha.Algebraic.size();i++) {
		if (k>0) { os << " "; }
		k++;
		os << lha.Algebraic[i]->eval(LhaFunc);
	}
	return os.str();
}

bool LHA::isFinal() {
	return FinalLoc[CurrentLocation];
}
bool LHA::isRejecting() {
	return RejectingLoc[CurrentLocation];
}
bool LHA::isPossible(std::map<std::string, double> watchedvars) {
	return CheckLocation( CurrentLocation, watchedvars );
}