/*******************************************************************************
 *                                                                             *
 * Cosmos:(C)oncept et (O)utils (S)tatistique pour les (Mo)deles               *
 * (S)tochastiques                                                             *
 *                                                                             *
 * Copyright (C) 2009-2012 LSV & LACL                                          *
 * Authors: Paolo Ballarini & Hilal Djafri                                     *
 * Website: http://www.lsv.ens-cachan.fr/Software/cosmos                       *
 *                                                                             *
 * This program is free software; you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * This program is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License along     *
 * with this program; if not, write to the Free Software Foundation, Inc.,     *
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.                 *
 *******************************************************************************
 */

#include "Lha-Reader.hpp"

#include "string.h"

#include <string>
#include <sstream>
#include <fstream>
#include <set>
#include <algorithm>
#include <list>




using namespace std;

LhaType::LhaType() : NbLoc(0),isDeterministic(true) {}


Lha_Reader::Lha_Reader(parameters &Q) : P(Q){
    trace_scanning = false;
    trace_parsing = false;

}

Lha_Reader::~Lha_Reader() {
}

string Lha_Reader::funDecl(const string& funtype) const{
    return "template<class DEDState>\n"+funtype+ " LHA<DEDState>::";
}

int Lha_Reader::parse(string& expr) {
    scan_expression(expr);

    lha::Lha_parser parser(*this);

    parser.set_debug_level(trace_parsing);

    int res = parser.parse();
    scan_end();
    return res;
}

int Lha_Reader::parse_file(parameters& P) {
    string str;
	MyLha.ConfidenceLevel=P.Level;

    ifstream file(P.PathLha.c_str(), ios::in);
    if (file) {


        while (!file.eof()) {

            string str2;
            getline(file, str2);
            str = str + "\n" + str2;
        }

        int x = parse(str);
        if (x) cout << "Parsing LHA description file failed" << endl;


        return x;
    } else {
        cout << "Can't open : " << P.PathLha << endl;
        return 1;
    }
}
/*
int Lha_Reader::parse_gml_file(parameters& P) {
	MyLha.ConfidenceLevel=P.Level;
    ifstream ifile(P.PathLha.c_str());
    if(ifile){
        //cout << "parse GML:" << filename << endl;
        ModelHandlerPtr handlerPtr(new MyLhaModelHandler(&MyLha,P));
        ExpatModelParser parser = ExpatModelParser(handlerPtr);
        parser.parse_file(P.PathLha);
        //cout << "end parse GML"<< endl;
        return 0;
    }else{
        cout << "File " << P.PathLha << " does not exist!" << endl;
        exit(EXIT_FAILURE);
    }


}
*/

bool is_simple(const string &s){
    return (s.find(" ") == string::npos
            && s.find("+") == string::npos
            && s.find("*") == string::npos
            && s.find("/") == string::npos
            && s.find("-") == string::npos
            && s.find(",") == string::npos
            && s.find("(") == string::npos
            && s.find(")") == string::npos);
}

string Lha_Reader::InvRelOp(const string& str)const {
    if (str == "<=") return ">=";
    if (str == ">=") return "<=";
	cerr << "Fail to inverse RelOp"<< endl;
	exit(EXIT_FAILURE);
}

void
Lha_Reader::error(const lha::location& l, const std::string& m) {
    std::cerr << l << ": " << m << std::endl;
}

void
Lha_Reader::error(const std::string& m) {
    std::cerr << m << std::endl;
}

void Lha_Reader::view() {


}











