#include "MarkingFormula.hpp"
#include <string>
#include <map>
#include <iostream>
#include <cmath>
#include <limits>
#include <vector>

MarkingFormula::MarkingFormula(MFType t, MarkingFormula* l,MarkingFormula* r){
    TypeOp = t;
    left = l;
    right = r;
}

MarkingFormula::MarkingFormula(MFType t, std::string str)
{
	TypeOp = t;
	varname = str;
}

MarkingFormula::MarkingFormula(MFType t, MarkingFormula* l)
{
	TypeOp = t;
	left = l;
}

MarkingFormula::MarkingFormula(int val)
{
	TypeOp = MF_INT;
	intval = val;
}

MarkingFormula::MarkingFormula(double val)
{
	TypeOp = MF_REAL;
	realval = val;
}

MarkingFormula::MarkingFormula()
{
	TypeOp = MF_EMPTY;
}

double MarkingFormula::eval(std::map<std::string, double> watchedvars, std::map<std::string, double> lhavars) {
	switch (TypeOp) {
		case MF_PLUS:
			return left->eval(watchedvars, lhavars) + right->eval(watchedvars, lhavars);
			break;
		case MF_MINUS:
			return left->eval(watchedvars, lhavars) - right->eval(watchedvars, lhavars);
			break;
		case MF_MULT:
			return left->eval(watchedvars, lhavars) * right->eval(watchedvars, lhavars);
			break;
		case MF_DIV:
			return left->eval(watchedvars, lhavars) / right->eval(watchedvars, lhavars);
			break;
		case MF_NEG:
			return 0-left->eval(watchedvars, lhavars);
		case MF_POW:
			return pow(left->eval(watchedvars, lhavars), right->eval(watchedvars, lhavars));
		case MF_FLOOR:
			return floor(left->eval(watchedvars, lhavars));
		case MF_MIN:
			{double le = left->eval(watchedvars, lhavars);
			double re = right->eval(watchedvars, lhavars);
			return (le < re ? le : re);}
			break;
		case MF_MAX:
			{double le = left->eval(watchedvars, lhavars);
			double re = right->eval(watchedvars, lhavars);
			return (le > re ? le : re);}
		case MF_MOD:
			std::cerr << "MOD is currently unsupported (MarkingFormula eval)" << std::endl;
			exit(1); return 0;
		case MF_REAL:
			return realval;
		case MF_INT:
			return intval;
		case HYBRIDVAR:
			std::cerr << "Hybrid Var currently unsupported (MarkingFormula eval)" << std::endl;
			exit(1); return 0;
			break;
		case WATCHEDVAR:
			if (watchedvars.find(varname) != watchedvars.end()) {
				return watchedvars[varname];
			} else {
				std::cerr << "Unknown Watched Variable:" << varname << std::endl;
				exit(1); return 0;
			}
			break;
		case LHAVAR:
			if (lhavars.find(varname) != lhavars.end()) {
				return lhavars[varname];
			} else {
				std::cerr << "Unknown LHA Variable:" << varname << std::endl;
				exit(1); return 0;
			}
			break;
		default:
			std::cerr << "Unable to evaluate a MarkingFormula because of operator " << TypeOp << std::endl;
			exit(1); return 0;
	}
}

ComparisonExpression::ComparisonExpression(CEType t, MarkingFormula* l,MarkingFormula* r){
	TypeOp = t;
	left =l;
	right = r;
}

ComparisonExpression::ComparisonExpression(CEType t, ComparisonExpression* l,ComparisonExpression* r){
	TypeOp = t;
	left_ce =l;
	right_ce = r;
}

ComparisonExpression::ComparisonExpression(CEType t, MarkingFormula* l){
	TypeOp = t;
	left =l;
}

ComparisonExpression::ComparisonExpression(CEType t, ComparisonExpression* l){
	TypeOp = t;
	left_ce =l;
}

ComparisonExpression::ComparisonExpression(CEType t){
	TypeOp = t;
}

ComparisonExpression::ComparisonExpression()
{
	TypeOp = CE_EMPTY;
}

bool ComparisonExpression::eval(std::map<std::string, double> watchedvars, std::map<std::string, double> lhavars)
{
	double le; double re; bool leb; bool reb;
	switch (TypeOp) {
		case CE_LEQ:{
			le=left->eval(watchedvars,lhavars);
			re=right->eval(watchedvars,lhavars);
			return (le <= re);
		}
		case CE_LL:{
			le=left->eval(watchedvars, lhavars);
			re=right->eval(watchedvars, lhavars);
			return (le < re);
		}
		case CE_GEQ:{
			le=left->eval(watchedvars,lhavars);
			re=right->eval(watchedvars,lhavars);
			return (le >= re);
		}
		case CE_GG:{
			le=left->eval(watchedvars, lhavars);
			re=right->eval(watchedvars, lhavars);
			return (le > re);
		}
		case CE_EQ:{
			le=left->eval(watchedvars, lhavars);
			re=right->eval(watchedvars, lhavars);
			return (le == re);
		}
		case CE_TRUE:
			return true;
		case CE_FALSE:
			return false;
		case CE_NOT:{
			leb=left_ce->eval(watchedvars, lhavars);
			return (not leb);
		}
		case CE_AND:{
			leb=left_ce->eval(watchedvars, lhavars);
			if (leb) { reb=right_ce->eval(watchedvars, lhavars);
					  return reb; }
			else { return false; }
		}
		case CE_OR:{
			leb=left_ce->eval(watchedvars, lhavars);
			if (leb) { return true; }
			else { return right_ce->eval(watchedvars, lhavars); }
		}
		default:
			std::cerr << "Unable to evaluate a ComparisonExpression because of operator " << TypeOp << std::endl;
			exit(1); return false;
	}
}

LHAFunc::LHAFunc(LFType t, MarkingFormula* l) {
	left=l;
	TypeOp=t;
}

double LHAFunc::startValue() {
	switch (TypeOp) {
		case LF_LAST:
			return 0;
		case LF_MIN:
			return std::numeric_limits<double>::max();
		case LF_MAX:
			return std::numeric_limits<double>::lowest();
		case LF_INT:
			return 0;
		case LF_MEAN:
			return 0;
		default:
			std::cerr << "Unknown LHA Func, identifier " << TypeOp << std::endl;
			exit(1); return 0;
			break;
	}
}

double LHAFunc::eval(double prev, double timediff, double tottime, std::map<std::string, double> watchedvars, std::map<std::string, double> lhavars) {
	double newvalue = left->eval(watchedvars, lhavars);
	switch (TypeOp) {
		case LF_LAST:
			return newvalue;
		case LF_MIN:
			return (prev < newvalue ? prev : newvalue);
		case LF_MAX:
			return (prev > newvalue ? prev : newvalue);
		case LF_INT:
			return prev + timediff * newvalue;
		case LF_MEAN:
			return (tottime - timediff)/tottime * prev + timediff/tottime * newvalue;
		default:
			std::cerr << "Unknown LHA Func (eval), identifier " << TypeOp << std::endl;
			exit(1); return 0;
			break;
	}
}

AlgebraicExpression::AlgebraicExpression(AEType t, int identifier)
{
	LhaFunc=identifier;
	TypeOp=t;
}

AlgebraicExpression::AlgebraicExpression(AEType t, AlgebraicExpression* l)
{
	TypeOp=t;
	left=l;
}

AlgebraicExpression::AlgebraicExpression(AEType t, AlgebraicExpression* l, AlgebraicExpression* r)
{
	TypeOp=t;
	left=l;
	right=r;
}

/*	AE_LhaFunc,
	AE_MIN,
	AE_MAX,
	AE_NEG,
	AE_FLOOR,
	AE_POW,
	AE_PLUS,
	AE_MINUS,
	AE_MULT,
	AE_DIV*/
double AlgebraicExpression::eval(std::vector<double> lhafunc) {
	switch (TypeOp) {
		case AE_LhaFunc:
			return lhafunc[LhaFunc];
		case AE_MIN:
			{double le = left->eval(lhafunc);
			 double re = right->eval(lhafunc);
			 return (le < re ? le : re);}
		case AE_MAX:
			{double le = left->eval(lhafunc);
			 double re = right->eval(lhafunc);
			 return (le > re ? le : re);}
		case AE_NEG:
			return 0 - left->eval(lhafunc);
		case AE_FLOOR:
			return floor(left->eval(lhafunc));
		case AE_POW:
			return pow(left->eval(lhafunc), right->eval(lhafunc));
		case AE_PLUS:
			return left->eval(lhafunc) + right->eval(lhafunc);
		case AE_MINUS:
			return left->eval(lhafunc) - right->eval(lhafunc);
		case AE_MULT:
			return left->eval(lhafunc) * right->eval(lhafunc);
		case AE_DIV:
			return left->eval(lhafunc) / right->eval(lhafunc);
		default:
			std::cerr << "Unknown Algebraic Expression, identifier " << TypeOp << std::endl;
			exit(1); return 0;
			break;
	}
}