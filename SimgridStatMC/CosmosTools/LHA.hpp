#include "MarkingFormula.hpp"
#include "LhaParser/Lha-Reader.hpp"
#include <set>
#include <vector>
#include <string>

#ifndef LHA_HH
#define LHA_HH

typedef std::pair <double, double> t_interval;

enum EdgeType {
	Auto, Synch
};

struct LhaEdgeBis {
	unsigned int Index;
	unsigned int Source;
	unsigned int Target;
	EdgeType Type;
	LhaEdgeBis():Index(0),Source(0),Target(0),Type(Auto){};
	LhaEdgeBis(unsigned int i, unsigned int s, unsigned int t,const EdgeType &ty):Index(i),Source(s),Target(t),Type(ty){};
};

struct _AutEdge {
	int Index;
	double FiringTime;
};
typedef struct _AutEdge AutEdge;

class LHA
{
	public:
	LHA(LhaType);
	
	const unsigned int NbLoc;
	int CurrentLocation;
	double CurrentTime;
	std::vector<double> FormulaVal;
	std::vector<bool> FormulaValQual;

	const size_t NbTrans;
	const size_t NbVar;
	
	protected:
	LhaType lha;
	/*std::vector<LhaEdgeBis> Edge;
	std::vector<int> EdgeCounter;*/
	set<int> InitLoc;
	vector<bool> FinalLoc;
	vector<bool> RejectingLoc;
	/*vector<set<int>> Out_A_Edges;*/
	//static const int ActionsEdgesAr[];
	
	std::map<std::string, double> lha_variables;
	vector<double> LhaFunc;
	
	void resetVariables();
	void DoElapsedTimeUpdate(double, std::map<std::string, double>); // Here std::map<std::string, double> or rather watchedvar replace the DEDS State.
	double GetFlow(int, std::map<std::string, double>);
	bool CheckLocation(int, std::map<std::string, double>);
	bool CheckEdgeConstraints(int, size_t, std::map<std::string, double>);
	t_interval GetEdgeEnablingTime(int, std::map<std::string, double>);
	void DoEdgeUpdates(int, std::map<std::string, double>); // Contains both DEDState & abstractBinding
	//void UpdateLinForm(std::map<std::string, double>);
	void UpdateLhaFunc(double, std::map<std::string, double>); // Was double alone but requires…
	//void UpdateFormulaVal(std::map<std::string, double>);
	
	public:
	// LHA_orig functions :
	void fireAutonomous(int, std::map<std::string, double>);
	//int synchroniseWith : probably doesn't have a use here
	AutEdge GetEnabled_A_Edges(std::map<std::string, double>);
	void updateLHA(double, std::map<std::string, double>);
	// void reset()
	// void getFinalValues()
	void fireLHA(int, std::map<std::string, double>);
	void setInitLocation(std::map<std::string, double>);
	// int GetEnabled_S_Edges
	
	// New?
	std::string GetAlgebraics();
	bool isFinal();
	bool isRejecting();
	bool isPossible(std::map<std::string, double>);
};
		
#endif