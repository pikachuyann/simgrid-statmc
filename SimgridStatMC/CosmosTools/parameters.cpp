#include "parameters.hpp"
#include <getopt.h>
#include <vector>
#include <string>
#include <cstring>

parameters::parameters() :
commandLine(""),
programName(""),
updatetime(100),
verbose(2),
SGExecutable(""),
Njob(1),

tmpPath("tmp"),
PathLha(""),

epsilon(0.000001),
continuousStep(1),
Level(0.99),
Width(0.001),
Batch(1000),
MinRuns(100),
MaxRuns(2000000),
sequential(true),
relative(false),
chernoff(false),

isTTY(true),
terminalWidth(80),

HaslFormulas(std::vector<HaslFormulasTop*>(0)),
HaslFormulasname(std::vector<std::string>(0)),
nbAlgebraic(0),
nbQualitatif(0)
{
}

void parameters::usage() {
	std::cout << "This is currently a work in progress." << std::endl;
	std::cout << "Usage: " << programName << " [options] <simulator> <platform> <deployment> <property>" << std::endl;
	std::cout << "General options:" << std::endl;
    std::cout << "\t--version\tdisplay version number" << std::endl;
    std::cout << "\t--njob    \tset the number of parallel threads" << std::endl;
	std::cout << "Option of simulation:" << std::endl;
    std::cout << "\t--level \tset the confidence level for the simulation (default=0.99)" << std::endl;
    std::cout << "\t--width \tset the width of the confidence interval (default=0.001)" << std::endl;
	std::cout << "\t--min-run \tset the minimum number of run (default=100)" << std::endl;
    std::cout << "\t--max-run \tset the maximal number of run (default=2000000)" << std::endl;
    std::cout << "\t--relative \tUse relative confidence interval instead of absolute one" << std::endl;
    std::cout << "\t--chernoff (level | width | nbrun)\tuse chernoff-hoeffding bound to compute the number of simulation" << std::endl;
}

Poption parameters::parsersingleOpt(int i)const{
    switch (i) {
		case 'h':
			return CO_help;
		case 'v':
			return CO_verbose;
		default:
			return (Poption)i;
    }
}

void parameters::parseCommandLine(int argc, char** argv) {
	programName = argv[0];
	commandLine = argv[0];
    for (int i = 1; i<argc; i++){
        commandLine += " ";
        commandLine += argv[i];
    }
	
	int c;
	
	while (1) {
        static struct option long_options[] ={
            /* Options for the simulator*/
            {"level",       required_argument, 0, CO_level},
            {"width",       required_argument, 0, CO_width},
			{"relative",    no_argument      , 0, CO_relative},
			{"min-run",		required_argument, 0, CO_min_run},
			{"max-run",     required_argument, 0, CO_max_run},
			{"chernoff",	required_argument, 0, CO_chernoff},

            /* Miscellaneous options */
			{"njob",        required_argument, 0, CO_njob},
            {"version",     no_argument      , 0, CO_version},

            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "gihspcrb:v:d:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) { break; }

		switch (parsersingleOpt(c)) {
			case CO_help:
				usage();
				exit(EXIT_SUCCESS);
				break;
			
			case CO_verbose:
				verbose = atoi(optarg);
				break;
				
			case CO_version:
				std::cout << "Work in progress" << std::endl;
				exit(0);
				break;
				
			case CO_level:
				Level = atof(optarg);
                break;
				
			case CO_relative:
				relative = true;
				break;
				
			case CO_width: {
                double w = atof(optarg);
                if(w==0.0)sequential=false;
                Width = w;
	            }
                break;
			
			case CO_min_run: MinRuns = atol(optarg);
				break;
			
			case CO_max_run: MaxRuns = atol(optarg);
				break;

			case CO_chernoff:
				chernoff = true;
				sequential = false;
				if (std::strcmp(optarg, "level") == 0)Level = 0;
				else if (std::strcmp(optarg, "width") == 0)Width = 0;
				else if (std::strcmp(optarg, "nbrun") == 0)MaxRuns = (unsigned long)-1;
				else {
					std::cerr << "The chernoff option requires one parameter (level | width | nbrun) to specify which should be computed" << std::endl;
					usage();
					exit(EXIT_FAILURE);
				}
				
			case CO_njob:
				Njob = atoi(optarg);
                break;
		}
	}
	
	if (optind + 4 > argc) {
		usage();
	} else {
		programName = argv[optind];
		platformFile = argv[optind+1];
		deploymentFile = argv[optind+2];
		PathLha = argv[optind+3];
	}
}
