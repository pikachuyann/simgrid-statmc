#ifndef __SGSMC_Parameters_
#define __SGSMC_Parameters_

#include <string>
#include <vector>
#include <chrono>
#include <map>

#include "HaslFormula.hpp"

#ifndef let
#define let const auto&
#endif

enum Poption {
	CO_level,
	CO_relative,
	CO_width,
	CO_min_run,
	CO_max_run,
	CO_chernoff,
	CO_verbose,
	CO_version,
	CO_help,
	CO_njob,
};

struct parameters {
	std::string commandLine;
	std::string programName;
	std::string platformFile;
	std::string deploymentFile;
	std::chrono::milliseconds updatetime;
	int verbose;
	int Njob;
	
	std::string SGExecutable;
	
	std::string tmpPath;
	std::string PathLha;
	
	double epsilon;
    int continuousStep;
    double Level;
    double Width;
    unsigned long int Batch;
    unsigned long int MaxRuns;
	unsigned long int MinRuns;
    bool sequential;
    bool relative;
    bool chernoff;
	
	bool isTTY;
	int terminalWidth;
	
	std::vector<HaslFormulasTop*> HaslFormulas;
    std::vector<std::string> HaslFormulasname;
	size_t nbAlgebraic;
    size_t nbQualitatif;
	
	parameters();
	void usage();
	void parseCommandLine(int argc, char** argv);
	Poption parsersingleOpt(int i) const;
};

extern parameters P;

#endif
