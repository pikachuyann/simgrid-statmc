#ifndef __Cosmos__MarkingFormula__
#define __Cosmos__MarkingFormula__

#include <string>
#include <map>
#include <vector>

enum MFType
{
	MF_EMPTY,
	MF_EQUAL,
	MF_PLUS,
	MF_MINUS,
	MF_MULT,
	MF_DIV,
	MF_ASSIGN,
	MF_NEG,
	MF_POW,
	MF_FLOOR,
	MF_MIN,
	MF_MAX,
	MF_MOD,
	MF_REAL,
	MF_INT,
	HYBRIDVAR,
	WATCHEDVAR,
	LHAVAR
};

class MarkingFormula
{
	public:
	MarkingFormula(MFType, std::string);
	MarkingFormula(MFType, MarkingFormula*, MarkingFormula*);
	MarkingFormula(MFType, MarkingFormula*);
	MarkingFormula(int);
	MarkingFormula(double);
	MarkingFormula();
	
	MFType TypeOp = MF_EMPTY;
	
	double eval(std::map<std::string,double>, std::map<std::string,double>);
	
	//private:
	MarkingFormula* left;
	MarkingFormula* right;
	std::string varname;
	int intval;
	double realval;
	
};

enum CEType
{
	CE_LEQ,
	CE_GEQ,
	CE_LL,
	CE_GG,
	CE_EQ,
	CE_TRUE,
	CE_FALSE,
	CE_NOT,
	CE_AND,
	CE_OR,
	CE_EMPTY
};

class ComparisonExpression {
	public:
	ComparisonExpression(CEType, MarkingFormula*, MarkingFormula*);
	ComparisonExpression(CEType, ComparisonExpression*, ComparisonExpression*);
	ComparisonExpression(CEType, MarkingFormula*);
	ComparisonExpression(CEType, ComparisonExpression*);
	ComparisonExpression(CEType);
	ComparisonExpression();
	
	CEType TypeOp;
		
	bool eval(std::map<std::string,double>, std::map<std::string,double>);
	
	//private:
	MarkingFormula* left;
	MarkingFormula* right;
	ComparisonExpression* left_ce;
	ComparisonExpression* right_ce;
};

enum LFType
{
	LF_LAST,
	LF_MIN,
	LF_MAX,
	LF_INT,
	LF_MEAN
};

class LHAFunc {
	public:
	LHAFunc(LFType, MarkingFormula*);
	
	LFType TypeOp;
	double startValue();
	double eval(double, double, double, std::map<std::string, double>, std::map<std::string, double>);
	
	MarkingFormula* left;
};

enum AEType
{
	AE_LhaFunc,
	AE_MIN,
	AE_MAX,
	AE_NEG,
	AE_FLOOR,
	AE_POW,
	AE_PLUS,
	AE_MINUS,
	AE_MULT,
	AE_DIV
};

class AlgebraicExpression {
	public:
	AlgebraicExpression(AEType, int);
	AlgebraicExpression(AEType, AlgebraicExpression*);
	AlgebraicExpression(AEType, AlgebraicExpression*, AlgebraicExpression*);
	
	double eval(std::vector<double>);
	AEType TypeOp;
	int LhaFunc;
	AlgebraicExpression* left;
	AlgebraicExpression* right;
};

#endif